import React, { Component } from 'react';
import AppRouter from './layout/router/Router';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  renderCustomer() {
    return (
      <div>
        <AppRouter />
      </div>
    );
  }

  render() {
    return (
      <div style={{ position: 'absolute', right: 0, top: 0, left: 0, }}>
        {this.renderCustomer()}
      </div>
    );
  }
}

export default App;

