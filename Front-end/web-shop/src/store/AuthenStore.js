import { observable, action } from "mobx";

class AuthenStore {
    @observable isAdmin = false;

    @observable isLogin = false;

    @observable token = null;

    @observable userInfor = null;

    @action
    setUserInfo(infor) {
        this.userInfor = infor;
    }

    @action
    setRole(bool) {
        this.isAdmin = bool;
    }

    @action
    setToken(token) {
        this.token = token;
    }
}

export default new AuthenStore();