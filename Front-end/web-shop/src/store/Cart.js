import { observable, action } from 'mobx';

class Cart {
    @observable items = [];

    @action
    setListProduct(items) {
        this.items = items;
    }

}

export default new Cart();