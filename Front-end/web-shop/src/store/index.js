import Header from './Header';
import Cart from './Cart';
import Detail from './Detail';
import AuthenStore from './AuthenStore';

export default {
    Header,
    Cart,
    Detail,
    AuthenStore
}