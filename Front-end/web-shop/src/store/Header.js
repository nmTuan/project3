import { observable, action } from "mobx";
import Config from '../config/Config';

class Header {
    @observable showModalLogin = false;

    @observable showModalRegister = false;

    @observable typeScreen = Config.typeScreen.home;

    @action
    setTypeScreen(type) {
        this.typeScreen = type;
    }

}

export default new Header();