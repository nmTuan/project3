import { observable, action } from 'mobx';

class Detail {
    @observable detail = null;

    @action
    setItem(item) {
        this.detail = item;
    }
}

export default new Detail();