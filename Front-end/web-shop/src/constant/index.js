export function formatMoney(value) {
    console.log(value.length)

    if (value.length <= 2) {
        return value;
    } else if (value.length === 5) {
        const preValue = value.slice(0, 2);
        const endValue = value.slice(2, value.length);
        return preValue + '.' + endValue;
    } else if (value.length === 6) {
        const preValue = value.slice(0, 3);
        const endValue = value.slice(3, value.length);
        return preValue + '.' + endValue;
    } else if (value.length === 7 || value.length === 8) {
        const preValue = value.slice(0, 2);
        const midValue = value.slice(2, 5);
        const endValue = value.slice(5, value.length);
        return preValue + '.' + midValue + '.' + endValue;
    } else if (value.length === 9) {
        const preValue = value.slice(0, 3);
        const midValue = value.slice(3, 6);
        const endValue = value.slice(6, value.length);
        return preValue + '.' + midValue + '.' + endValue;
    }
}