import React, { Component } from 'react';
import { Layout, Button, Menu, Icon } from 'antd';
import { inject, observer } from 'mobx-react';

import Config from '../../config/Config';
import { Link } from 'react-router-dom';

const { Header, Sider, Content } = Layout;

@inject('AuthenStore')
@observer
class BaseLayout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openKey: [],
        };
    }

    onOpenChange = openKeys => {
        this.setState({
            openKey: openKeys
        });
    }

    onClickItem = item => {
        this.props.onClickItem(item);
    }

    renderSider(item, index) {
        return (
            <Menu.Item onClick={this.onClickItem} style={styles.menuItem} key={index.toString()}>
                <div style={{ marginLeft: 10, color: '#000000' }}>{item.name}</div>
            </Menu.Item>
        );
    }

    renderComponent() {
        return null;
    }

    onClickLogout = () => {
        const { AuthenStore } = this.props;
        AuthenStore.isAdmin = false;
        AuthenStore.isLogin = false;
    }

    render() {
        const { openKey } = this.state;
        return (
            <div>
                <Layout style={styles.container}>
                    <Sider
                        width={300}
                        style={styles.backgroundSider}
                    >
                        <div style={styles.viewSider}>
                            <h1>Quản lý thông tin</h1>
                            <div style={styles.containerMenu}>
                                <Menu
                                    mode='inline'
                                    openKeys={openKey}
                                    onOpenChange={this.onOpenChange}
                                    defaultSelectedKeys='0'
                                    style={styles.menu}
                                >
                                    {Config.listFunction.map((item, index) => this.renderSider(item, index))}
                                </Menu>
                            </div>
                        </div>
                    </Sider>
                    <Layout style={{ flex: 1 }}>
                        <Header style={{ backgroundColor: '#ED1A24', height: 80, display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                            <div style={{ width: 60, height: 60, borderRadius: 30, backgroundColor: '#F5F6FC', display: 'flex', overflow: 'hidden', alignItems: 'center', justifyContent: 'center' }}>
                                <Icon type='user' style={{ fontSize: 40 }} />
                            </div>
                            <Button type='link' onClick={this.onClickLogout}>
                                <Link to='/'>
                                    <Icon type='logout' style={{ fontSize: 20, color: '#000000' }} />
                                </Link>
                            </Button>
                        </Header>
                        <Content style={{ height: '90vh', overflow: 'auto' }}>{this.props.children}</Content>
                    </Layout>
                </Layout>
            </div>
        );
    }
}

const styles = {
    container: { flex: 1 },
    backgroundSider: { backgroundColor: '#F5F6FC', height: '100vh', boxShadow: '-20px 10px 40px 0px #c0cbd1 inset', },
    viewSider: { flex: 1, backgroundColor: '#F5F6FC', boxShadow: '-20px 0px 40px 0px #c0cbd1 inset', paddingLeft: 10, paddingTop: 30 },
    containerMenu: { paddingTop: 20, alignItems: 'center' },
    menu: { backgroundColor: 'transparent' },
    titleSubMenu: { color: '#000000', marginLeft: 10 },
    menuItem: { display: 'flex', alignItems: 'center' },
};

export default BaseLayout;