import { Request } from "../config/Request"

export const HomeService = {
    getNewProduct: async () => {
        const res = await Request.getWithoutParam('newProduct');
        return res;
    },
    getProductForHome: async (param, pageIndex, sizeOfPage) => {
        const params = {
            CategoryName: param,
            pageIndex,
            sizeOfPage
        }
        const res = await Request.get('productForHome', params);
        return res;
    },
    getDetailProduct: async (id) => {
        const res = await Request.getWithoutParam(`detailProduct/${id}`);
        return res;
    }

}