import { Request } from "../config/Request"
import api from "../config/api"

export const Authentication = {
    login: async (email, password) => {
        const body = {
            email,
            password
        }
        const res = await Request.post(api.login, body);
        return res;
    },
    singup: async (name, email, password, phone, address) => {
        const body = {
            name,
            email,
            password,
            phone,
            address: address || '',
        }
        const res = await Request.post(api.register, body);
        if (res) {
            return res;
        }
    },
    getUserInfor: async (token) => {
        const res = await Request.getWithToken(api.userInfo, token);
        return res.data;
    }
}