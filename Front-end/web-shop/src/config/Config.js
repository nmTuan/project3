export default {
    domain: 'http://localhost:9999/api',
    typeScreen: {
        laptop: 'Laptop',
        phone: 'Điện thoại',
        tablet: 'Tablet/Ipad',
        accessories: 'Phụ kiện',
        home: 'Home'
    },
    listFunction: [
        {
            id: 1, name: 'Quản lý sản phẩm'
        },
        {
            id: 2, name: 'Quản lý tài khoản'
        },
    ]

}