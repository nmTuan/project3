export default {
    login: '/auth/login',
    register: '/signUp',
    userInfo: '/userInfo',
}