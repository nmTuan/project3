import Config from './Config';
import Axios from 'axios';

export const Request = {
    async Header() {
        return await Axios.create({
            baseURL: Config.domain,
            timeout: 1000,
            headers: { 'Content-Type': 'application/json' }
        })
    },
    async HeaderWithToken(token) {
        return await Axios.create({
            baseURL: Config.domain,
            timeout: 1000,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
    },
    async post(url, body) {
        try {
            const api = await this.Header();
            const res = await api.post(url, body);
            return res.data;
        } catch (err) {
            console.log('err', err)
        }
    },

    async postWithToken(token, url, body, ) {
        try {
            const api = await this.HeaderWithToken(token);
            const res = await api.post(url, body);
            return res.data;
        } catch (err) {
            console.log('err', err)
        }
    },
    async getWithoutParam(url) {
        try {
            const api = await this.Header();
            const res = await api.get(url);
            //console.log('RES', JSON.stringify(res));

            return res.data;
        } catch (error) {
            console.error(error);
        }
    },
    async get(url, params) {
        console.log('PARAMS', JSON.stringify(params))
        try {
            const api = await this.Header();
            const res = await api.get(url, {
                params
            });
            console.log('RESPONSE', res.data)
            return res.data;
        } catch (error) {
            console.error(error);
        }
    },
    async getWithToken(url, token) {
        try {
            const api = await this.HeaderWithToken(token);
            const res = api.get(url);
            return res;
        } catch (error) {
            console.error(error);
        }
    }
}