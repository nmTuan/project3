import React, { Component } from 'react';
import { Col, Input, Select, Button, Icon } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import BaseLayout from '../../screenAmin/BaseContent';

const { Option } = Select;

const styles = {
    styleInput: { width: '70%', paddingTop: 10 },
    container: { marginTop: 20, }
}
class AddProduct extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    onCLickBack = () => {
        const { history } = this.props;
        history.push('/manager')
    }

    renderComponent() {
        return (
            <div style={{ display: 'flex', alignItems: 'center', flexDirection: 'column' }}>
                <Col offset={4} span={16}>
                    <Col span={12} style={styles.container}>
                        <div>
                            <div>Tên sản phẩm</div>
                            <div style={styles.styleInput}>
                                <Input placeholder='Tên sản phẩm' />
                            </div>
                        </div>
                    </Col>
                    <Col span={12} style={styles.container}>
                        <div>
                            <div>Giá sản phẩm</div>
                            <div style={styles.styleInput}>
                                <Input placeholder='Giá sản phẩm' />
                            </div>
                        </div>
                    </Col>

                    <Col span={12} style={styles.container}>
                        <div>
                            <div>Số lượng sản phẩm</div>
                            <div style={styles.styleInput}>
                                <Input placeholder='Số lượng sản phẩm' />
                            </div>
                        </div>
                    </Col>
                    <Col span={12} style={styles.container}>
                        <div>
                            <div>Giảm giá (%)</div>
                            <div style={styles.styleInput}>
                                <Input placeholder='%' />
                            </div>
                        </div>
                    </Col>
                    <Col span={12} style={styles.container}>
                        <div>
                            <div>Loại sản phẩm</div>
                            <div style={styles.styleInput}>
                                <Select placeholder='Loại sản phẩm' style={{ width: '100%', }}>
                                    <Option value='phone'>Điện thoại</Option>
                                    <Option value='laptop'>Máy tính</Option>
                                    <Option value='Tablet'>Tablet/Ipad</Option>
                                    <Option value='accessories'>Phụ kiện</Option>
                                </Select>
                            </div>
                        </div>
                    </Col>
                    <Col span={12} style={styles.container}>
                        <div>
                            <div>Mô tả</div>
                            <div style={styles.styleInput}>
                                <TextArea placeholder='Mô tả' autosize />
                            </div>
                        </div>
                    </Col>
                    <Col span={24} style={styles.container}>
                        <div>Ảnh minh hoạ</div>
                        <div style={styles.styleInput}>
                            <Button type='dashed' style={{ width: 200, height: 200 }}>
                                <Icon type='plus-square' style={{ fontSize: 40 }} />
                            </Button>
                        </div>
                    </Col>
                </Col>
                <div style={{ marginTop: 40 }}>
                    <Button onClick={this.onCLickBack}>Quay lại</Button>
                    <Button type='primary' style={{ marginLeft: 30 }}>Thêm sản phẩm</Button>
                </div>
            </div>
        );
    }
    render() {
        return (
            <BaseLayout>
                {this.renderComponent()}
            </BaseLayout>
        );
    }
}

export default AddProduct;
