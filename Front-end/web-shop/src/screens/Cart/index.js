/* eslint-disable array-callback-return */
import React, { Component } from 'react';
import { Col, Button, Input, Icon } from 'antd';
import { Link } from 'react-router-dom';
import { inject, observer } from 'mobx-react';

import { formatMoney } from '../../constant';
import BaseContentCustomer from '../BaseContentCustomer.js';

@inject('Cart')
@observer
class index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [
                { id: 1, image: 'http://laptop88.vn/media/product/pro_poster_4847.jpg', name: 'Asus123', price: '12000000', discount: 0 },
                { id: 2, image: 'http://laptop88.vn/media/product/pro_poster_4847.jpg', name: 'Asus123', price: '12000000', discount: 10 },
                { id: 3, image: 'http://laptop88.vn/media/product/pro_poster_4847.jpg', name: 'Asus123', price: '12000000', discount: 0 },
                { id: 4, image: 'http://laptop88.vn/media/product/pro_poster_4847.jpg', name: 'Asus123', price: '12000000', discount: 0 },
                { id: 5, image: 'http://laptop88.vn/media/product/pro_poster_4847.jpg', name: 'Asus123', price: '12000000', discount: 20 },
                { id: 6, image: 'http://laptop88.vn/media/product/pro_poster_4847.jpg', name: 'Asus123', price: '12000000', discount: 0 },
            ],
            priceTerm: 0,
            totalAllProduct: 0,
        };
    }

    componentDidMount() {
        this.calculatePrice();
    }

    calculatePrice = () => {
        const { Cart } = this.props;
        let total = 0;
        Cart.items.map(item => {
            const priceItem = item.amount * item.product.price;
            total += priceItem;
        })
        this.setState({
            priceTerm: total,
            totalAllProduct: total + 0
        })
    }

    onClickIncrease = (index) => {
        const { Cart } = this.props;
        Cart.items[index].amount += 1;
        this.calculatePrice();
    }

    onClickBuy = () => {
        alert('Giỏ hàng không có sản phẩm');
    }

    onClickDecrease = (index) => {
        const { Cart } = this.props;
        if (Cart.items[index].amount > 1) {
            Cart.items[index].amount -= 1;
            this.calculatePrice();
        }
    }

    onCLickDelete = (item, index) => {
        const { Cart } = this.props;
        Cart.items.splice(index, 1);
    }

    renderItems(item, index) {
        return (
            <div style={{ width: '100%', borderColor: '#000000', borderWidth: 1, borderStyle: 'solid', borderRadius: 5, marginTop: 10 }}>
                <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', padding: 10 }}>
                    <div style={{ flex: 1 }}>
                        <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center' }}>
                            <img src={item.image} alt='Product' height={120} width={120} />
                            <div>{item.product.nameProduct}</div>
                        </div>
                    </div>
                    <div style={{ flex: 1, display: 'flex', justifyContent: 'center' }}>{`${formatMoney(item.product.price.toString())} đ`}</div>
                    <div style={{ flex: 1, display: 'flex', justifyContent: 'center' }}>
                        <Input
                            style={{ textAlign: 'center', padding: 12 }}
                            value={item.amount}
                            suffix={<Button onClick={() => this.onClickIncrease(index)}>+</Button>}
                            prefix={<Button onClick={() => this.onClickDecrease(index)}>-</Button>} />
                    </div>
                    <div>
                        <Button onClick={() => this.onCLickDelete(item, index)}>
                            <Icon type='delete' />
                        </Button>
                    </div>
                </div>
            </div >
        );
    }

    render() {
        const { Cart } = this.props;
        const { totalAllProduct, priceTerm } = this.state;
        return (
            <div>
                <BaseContentCustomer>
                    <div style={{ paddingTop: 20 }}>
                        <Col offset={2} span={12}>
                            <div >
                                <div style={{ display: 'flex', flexDirection: 'row', backgroundColor: 'rgb(42, 236, 178)', padding: 10 }}>
                                    <div style={{ flex: 1 }}>Tên sản phẩm</div>
                                    <div style={{ flex: 1, display: 'flex', justifyContent: 'center' }}>Giá</div>
                                    <div style={{ flex: 1, display: 'flex', justifyContent: 'center' }}>Số lượng</div>
                                </div>
                                <div>
                                    {
                                        Cart.items.length > 0 ?
                                            Cart.items.map((item, index) => this.renderItems(item, index))
                                            :
                                            <div>Không có sản phẩm</div>
                                    }
                                </div>
                            </div>
                        </Col>
                        <Col offset={1} span={7} style={{ backgroundColor: 'blue' }}>
                            <div style={{ backgroundColor: 'rgb(42, 236, 178)', padding: 5, paddingBottom: 20, paddingLeft: 10, paddingRight: 10 }}>
                                <div>
                                    Thông tin sản phẩm
                        </div>
                                <div style={{ backgroundColor: 'gray', height: 1, width: '100%', marginTop: 15, marginBottom: 15 }} />
                                <div>{`Tạm tính: ${formatMoney(priceTerm.toString())} đ`}</div>
                                <div style={{ paddingTop: 10 }}>{`Phí giao hàng: 0 đ`}</div>
                                <div style={{ backgroundColor: 'gray', height: 1, width: '100%', marginTop: 15, marginBottom: 15 }} />
                                <div>Tổng cộng</div>
                                <div style={{ color: 'red', paddingTop: 5 }}>{`${formatMoney(totalAllProduct.toString())} đ`}</div>
                                <div style={{ backgroundColor: 'gray', height: 1, width: '100%', marginTop: 15, marginBottom: 15 }} />
                                <div style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
                                    {
                                        this.props.Cart.items.length > 0 ?
                                            <Button>
                                                <Link to={{
                                                    pathname: "/buy",
                                                }}>
                                                    Xác nhận giỏ hàng
                                        </Link>
                                            </Button>
                                            :
                                            <Button onClick={this.onClickBuy}>
                                                <div>Xác nhận giỏ hàng</div>
                                            </Button>
                                    }
                                </div>
                            </div>
                        </Col>
                    </div>
                </BaseContentCustomer>
            </div>
        );
    }
}

export default index;
