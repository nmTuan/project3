import React, { Component } from 'react';
import { Input, Icon, Checkbox, Button } from 'antd';
import { Link } from 'react-router-dom';
import { inject, observer } from 'mobx-react';

import "./styles.css"
import { Authentication } from '../../services/Authentication';

@inject('Header', 'AuthenStore')
@observer
class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isCheck: false,
            username: '',
            password: '',
        };
    }

    handleCheck = () => {
        this.setState({
            isCheck: !this.state.isCheck
        })
    }

    onChangeUsername = event => {
        this.setState({
            username: event.target.value
        })
    }

    onChangePass = event => {
        this.setState({
            password: event.target.value
        })
    }

    onClickLogin = async () => {
        const { AuthenStore, Header, history } = this.props;
        const { username, password } = this.state;
        if (username !== '' && password !== '') {
            const isAdmin = username.toUpperCase().search('ADMIN');
            const res = await Authentication.login(username, password);
            if (res) {
                AuthenStore.setToken(res); 
                const userInfor = await Authentication.getUserInfor(res.jwt);
                if (userInfor) {
                    AuthenStore.setUserInfo(userInfor)
                }
                if (isAdmin === -1) {
                    AuthenStore.isAdmin = false;
                    AuthenStore.isLogin = true;
                    Header.showModalLogin = false;
                } else {
                    AuthenStore.isAdmin = true;
                    AuthenStore.isLogin = true;
                    history.push('/manager')
                    Header.showModalLogin = false;
                }
               
            }
        } else {
            alert('Vui lòng điền đầy đủ thông tin')
        }
    }

    onCLickRegister = () => {
        const { Header } = this.props;
        Header.showModalLogin = false;
        Header.showModalRegister = true;
    }

    render() {
        const { isCheck, username, password } = this.state;
        return (
            <div>
                <div style={{ padding: 15 }}>
                    <div style={{ color: '#000000', fontSize: 24, fontWeight: 'bold' }}>Đăng nhập</div>
                    <div style={{ paddingTop: 15 }}>
                        <div >
                            <Input
                                prefix={
                                    <div style={{ maxHeight: '100%', width: 40, margin: 0, paddingBottom: 0 }}>
                                        <Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />
                                    </div>}
                                placeholder='Username'
                                value={username}
                                onChange={this.onChangeUsername}
                            />
                        </div>
                        <div style={{ paddingTop: 8 }}>
                            <Input
                                prefix={
                                    <div style={{ maxHeight: '100%', width: 40, margin: 0, paddingBottom: 0 }}>
                                        <Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />
                                    </div>}
                                placeholder='Password'
                                value={password}
                                type='password'
                                onChange={this.onChangePass}
                            />
                        </div>
                        <div style={{ paddingTop: 8 }}>
                            <Checkbox
                                checked={isCheck}
                                style={{ color: '#34d2eb' }}
                                onChange={this.handleCheck}
                            >
                                Nhớ tài khoản
                            </Checkbox>
                        </div>
                    </div>
                    <div style={{ width: '100%', display: 'flex', justifyContent: 'flex-end', paddingTop: 10 }}>
                        <Button
                            style={{ backgroundColor: '#0cf22f', borderColor: '#0cf22f' }}
                            onClick={this.onClickLogin}
                        >Đăng nhập</Button>
                    </div>
                </div>

                <div style={{ width: '100%', height: 1, backgroundColor: '#00000060', marginTop: 20 }} />
                <div style={{ width: '100%', display: 'flex', justifyContent: 'center', paddingTop: 10, paddingBottom: 8 }}>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <div style={{ color: '#000000' }}>Chưa có tài khoản?</div>
                        <Link style={{ marginLeft: 3, color: '#0cdff2' }} onClick={this.onCLickRegister}>Đăng ký</Link>
                    </div>
                </div>
                <div style={{ width: '100%', display: 'flex', justifyContent: 'center', paddingBottom: 8 }}>
                    <Link style={{ marginLeft: 3, color: '#0cdff2' }}>Quên mật khẩu</Link>
                </div>
            </div>
        );
    }
}

export default Login;
