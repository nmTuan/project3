import React, { Component } from 'react';
import { Table, Button, Icon } from 'antd';
import { inject, observer } from 'mobx-react';
import { Link } from 'react-router-dom';
import Search from 'antd/lib/input/Search';
import BaseLayout from '../../screenAmin/BaseContent';

const columns = [
    {
        title: 'STT',
        render: (text, record, index) => <div>{index + 1}</div>,
    },
    {
        title: 'Tên sản phẩm',
        dataIndex: 'pname',
    },
    {
        title: 'Hình ảnh',
        dataIndex: 'image',
        render: text => (
            <div style={{ width: 80, height: 80, backgroundSize: '100% 100%', backgroundRepeat: 'no-repeat', backgroundImage: `url(${text})` }} />
        )
    },
    {
        title: 'Giá',
        dataIndex: 'price',
    },
    {
        title: 'Số lượng',
        dataIndex: 'quantity',
    },
    {
        title: 'Giảm giá',
        dataIndex: 'discount',
    },
    {
        title: 'Loại sản phẩm',
        dataIndex: 'type',
    },
    {
        title: 'Mô tả',
        dataIndex: 'descript',
        width: '20%'
    },
    {
        title: 'Ngày sửa đổi',
        dataIndex: 'dateModify',
    },
    {
        title: 'Thao tác',
        width: '5%',
        render: (text, record, index) => (
            <div style={{ display: 'flex', alignItems: 'center' }}>
                <Button type="link">
                    <Link
                        to={{
                            pathname: '/group-2/InformationAcc',
                            state: { updateScreen: true },
                        }}
                    >
                        <Icon type="edit" />
                    </Link>
                </Button>
                <Button type="link" onClick={() => onClickDelete(index)}>
                    <Icon type="delete" />
                </Button>
            </div>
        ),
    },
];

let data = [
    {
        key: '1',
        pname: 'Asus123',
        image: 'http://laptop88.vn/media/product/pro_poster_4847.jpg',
        price: 12000000,
        quantity: 1,
        discount: 30,
        type: 'laptop',
        descript: 'Test',
        dateModify: '10/12/2019'
    },
    {
        key: '2',
        pname: 'Asus123',
        image: 'http://laptop88.vn/media/product/pro_poster_4847.jpg',
        price: 12000000,
        quantity: 3,
        discount: 30,
        type: 'laptop',
        descript: 'Test',
        dateModify: '10/12/2019'
    },
    {
        key: '3',
        pname: 'Asus123',
        image: 'http://laptop88.vn/media/product/pro_poster_4847.jpg',
        price: 12000000,
        quantity: 3,
        discount: 30,
        type: 'laptop',
        descript: 'Test',
        dateModify: '10/12/2019'
    },
    {
        key: '4',
        pname: 'Asus123',
        image: 'http://laptop88.vn/media/product/pro_poster_4847.jpg',
        price: 12000000,
        quantity: 3,
        discount: 30,
        type: 'laptop',
        descript: 'Test',
        dateModify: '10/12/2019'
    },
    {
        key: '5',
        pname: 'Asus123',
        image: 'http://laptop88.vn/media/product/pro_poster_4847.jpg',
        price: 12000000,
        quantity: 3,
        discount: 30,
        type: 'laptop',
        descript: 'Test',
        dateModify: '10/12/2019'
    },
    {
        key: '6',
        pname: 'Asus123',
        image: 'http://laptop88.vn/media/product/pro_poster_4847.jpg',
        price: 12000000,
        quantity: 3,
        discount: 30,
        type: 'laptop',
        descript: 'Test',
        dateModify: '10/12/2019'
    },
    {
        key: '7',
        pname: 'Asus123',
        image: 'http://laptop88.vn/media/product/pro_poster_4847.jpg',
        price: 12000000,
        quantity: 3,
        discount: 30,
        type: 'laptop',
        descript: 'Test',
        dateModify: '10/12/2019'
    },
    {
        key: '8',
        pname: 'Asus123',
        image: 'http://laptop88.vn/media/product/pro_poster_4847.jpg',
        price: 12000000,
        quantity: 3,
        discount: 30,
        type: 'laptop',
        descript: 'Test',
        dateModify: '10/12/2019'
    },
    {
        key: '9',
        pname: 'Asus123',
        image: 'http://laptop88.vn/media/product/pro_poster_4847.jpg',
        price: 12000000,
        quantity: 3,
        discount: 30,
        type: 'laptop',
        descript: 'Test',
        dateModify: '10/12/2019'
    },
    {
        key: '10',
        pname: 'Asus123',
        image: 'http://laptop88.vn/media/product/pro_poster_4847.jpg',
        price: 12000000,
        quantity: 3,
        discount: 30,
        type: 'laptop',
        descript: 'Test',
        dateModify: '10/12/2019'
    },
    {
        key: '11',
        pname: 'Asus123',
        image: 'http://laptop88.vn/media/product/pro_poster_4847.jpg',
        price: 12000000,
        quantity: 3,
        discount: 30,
        type: 'laptop',
        descript: 'Test',
        dateModify: '10/12/2019'
    },
    {
        key: '12',
        pname: 'Asus123',
        image: 'http://laptop88.vn/media/product/pro_poster_4847.jpg',
        price: 12000000,
        quantity: 3,
        discount: 30,
        type: 'laptop',
        descript: 'Test',
        dateModify: '10/12/2019'
    },
    {
        key: '13',
        pname: 'Asus123',
        image: 'http://laptop88.vn/media/product/pro_poster_4847.jpg',
        price: 12000000,
        quantity: 3,
        discount: 30,
        type: 'laptop',
        descript: 'Test',
        dateModify: '10/12/2019'
    },
    {
        key: '14',
        pname: 'Asus123',
        image: 'http://laptop88.vn/media/product/pro_poster_4847.jpg',
        price: 12000000,
        quantity: 3,
        discount: 30,
        type: 'laptop',
        descript: 'Test',
        dateModify: '10/12/2019'
    },
];

function onClickDelete(index) {
    alert(index);
}

@inject('AuthenStore')
@observer
class ManageProduct extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount(){
        const { AuthenStore } = this.props;
        console.log('asdf', JSON.stringify(AuthenStore.userInfor));
        
    }


    onClickAdd = () => {
        const { history } = this.props;
        history.push('/addProduct')
    }

    onClickItem = (item) => {
        console.log('----', item);
    }

    renderComponent() {
        return (
            <div style={{ padding: 10 }}>
                <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between', paddingBottom: 20 }}>
                    <div style={{ width: 500 }}>
                        <Search
                            placeholder='Nhập tên sản phẩm'
                            enterButton='Search'
                            size='large'
                        />
                    </div>
                    <div>
                        <Button type='primary' onClick={this.onClickAdd}>
                            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                                <Icon type='plus-square' />
                                <div style={{ marginLeft: 10 }}>Thêm sản phẩm</div>
                            </div>
                        </Button>
                    </div>
                </div>
                <Table columns={columns} dataSource={data} bordered rowKey='key' />
            </div>
        );
    }
    render() {
        return (
            <BaseLayout>
                {this.renderComponent()}
            </BaseLayout>
        );
    }
}

export default ManageProduct;
