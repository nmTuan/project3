import React, { Component } from 'react';
import TypeProduct from '../TypeProduct';
import { HomeService } from '../../services/HomeService';
import { inject, observer } from 'mobx-react';

@inject('Detail')
@observer
class Phone extends Component {
    constructor(props) {
        super(props);
        this.state = {
            arr: [],
            listCurrentProduct: [],
            sumProduct: 0
        };
    }

    async componentDidMount() {
        const listPhone = await HomeService.getProductForHome('PHONE', 0, 20);
        if (listPhone) {
            this.setState({
                arr: listPhone,
                listCurrentProduct: listPhone,
                sumProduct: listPhone[0].sumProduct
            });
        }
    }

    handleChangePage = (page, pageSize) => {
        const arr = [];
        const size = (page - 1) * 20;
        const total = page * 20;
        if (total < this.state.sumProduct) {
            for (let i = size; i < total; i++) {
                arr.push(i);
            }
        } else {
            for (let i = size; i < this.state.sumProduct; i++) {
                arr.push(i);
            }
        }
        this.setState({
            listCurrentProduct: arr
        })
    }

    onClickItem = (item) => {
        const { Detail, history } = this.props;
        Detail.setItem(item);
        history.push('/detail')
    }

    render() {
        const { listCurrentProduct, sumProduct } = this.state;
        return (
            <div>
                <TypeProduct
                    handleChangePage={this.handleChangePage}
                    listCurrentProduct={listCurrentProduct}
                    sumProduct={sumProduct}
                    onClickItem={this.onClickItem}
                />
            </div>
        );
    }
}

export default Phone;
