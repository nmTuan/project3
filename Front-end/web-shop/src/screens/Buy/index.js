import React, { Component } from 'react';
import { Input, Radio, Button } from 'antd';
import { formatMoney } from '../../constant';
import BaseContentCustomer from '../BaseContentCustomer.js';

class Buy extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 1
        };
    }

    handleSelect = (e) => {
        this.setState({
            value: e.target.value
        })
    }

    renderChild() {
        const { value } = this.state;
        return (
            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', paddingTop: 40 }}>
                <h1>Thông tin đặt hàng</h1>
                <div style={{ boxShadow: '1px 2px 7px #000000', borderRadius: 5, width: '30%', padding: 10, paddingLeft: 30, paddingRight: 30 }}>
                    <Input
                        style={{ marginTop: 10 }}
                        placeholder='Họ và tên *'
                    />
                    <Input
                        style={{ marginTop: 10 }}
                        placeholder='Số điện thoại *'
                    />
                    <Input
                        style={{ marginTop: 10 }}
                        placeholder='Địa chỉ *'
                    />
                    <Input
                        style={{ marginTop: 10 }}
                        placeholder='email'
                    />
                    <div style={{ width: '100%', paddingTop: 20 }}>
                        <div>Hình thức thanh toán</div>
                        <Radio.Group
                            style={{ width: '100%', display: 'flex', justifyContent: 'space-between', marginTop: 5 }}
                            value={value}
                            onChange={this.handleSelect}
                        >

                            <Radio value={1}>
                                COD
                            </Radio>
                            <Radio value={2}>
                                Thanh toán online
                            </Radio>
                        </Radio.Group>
                    </div>
                    <div style={{ display: 'flex', justifyContent: 'center', paddingTop: 30 }}>
                        <Button type='danger' style={{ width: '50%' }}>
                            Đặt hàng
                        </Button>
                    </div>
                </div>
                <div style={{ borderWidth: 1, borderStyle: 'solid', borderRadius: 5, borderColor: '#d0d7db', width: '30%', marginTop: 20, padding: 10, paddingLeft: 30, paddingRight: 30 }}>
                    <div style={{ display: 'flex', justifyContent: 'center' }}>
                        Thông tin sản phẩm
                        </div>
                    <div style={{ backgroundColor: 'gray', height: 1, width: '100%', marginTop: 15, marginBottom: 15 }} />
                    <div>
                        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                            <div>Ausus123 - x1</div>
                            <div>{`${formatMoney('19000000')} đ`}</div>
                        </div>
                    </div>
                    <div style={{ backgroundColor: 'gray', height: 1, width: '100%', marginTop: 15, marginBottom: 15 }} />
                    <div>{`Tạm tính: ${formatMoney('19000000')} đ`}</div>
                    <div style={{ paddingTop: 10 }}>{`Phí giao hàng: 0 đ`}</div>
                    <div style={{ backgroundColor: 'gray', height: 1, width: '100%', marginTop: 15, marginBottom: 15 }} />
                    <div>Tổng cộng</div>
                    <div style={{ color: 'red', paddingTop: 5 }}>{`${formatMoney('19000000')} đ`}</div>
                </div>
            </div>
        );
    }
    render() {
        return (
            <div>
                <BaseContentCustomer>
                    {this.renderChild()}
                </BaseContentCustomer>
            </div>
        );
    }
}

export default Buy;
