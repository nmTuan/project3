import React, { Component } from 'react';
import { Input, Button } from 'antd';
import { inject, observer } from 'mobx-react';

import './Detail.css';
import { formatMoney } from '../../constant';
import BaseContentCustomer from '../BaseContentCustomer.js';
import { HomeService } from '../../services/HomeService';

@inject('Cart', 'Detail')
@observer
class index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentItem: 0,
            value: 1,
            data: null,
            otherProduct: [
                {
                    id: '1',
                    nameProduct: 'Tên sản phẩm',
                    price: 1800000,
                    bigImg: 'https://salt.tikicdn.com/cache/550x550/ts/product/a5/b1/03/60de1319622d3775ed78fa2199dcb317.jpg',
                    smallImg: 'https://salt.tikicdn.com/cache/75x75/ts/product/a5/b1/03/60de1319622d3775ed78fa2199dcb317.jpg'
                },
                {
                    id: '2',
                    nameProduct: 'Tên sản phẩm',
                    price: 1800000,
                    promotion: 15,
                    bigImg: 'https://salt.tikicdn.com/cache/550x550/ts/product/98/0a/a6/9d9d1e30d026b166a0e90e83010f81d9.jpg',
                    smallImg: 'https://salt.tikicdn.com/cache/75x75/ts/product/98/0a/a6/9d9d1e30d026b166a0e90e83010f81d9.jpg'
                },
                {
                    id: '3',
                    nameProduct: 'Tên sản phẩm',
                    price: 1800000,
                    promotion: 20,
                    bigImg: 'https://salt.tikicdn.com/cache/550x550/ts/product/82/71/e8/e733b686fb297f235ac2c86482fa3547.jpg',
                    smallImg: 'https://salt.tikicdn.com/cache/75x75/ts/product/82/71/e8/e733b686fb297f235ac2c86482fa3547.jpg'
                },
                {
                    id: '4',
                    nameProduct: 'Tên sản phẩm',
                    price: 1800000,
                    bigImg: 'https://salt.tikicdn.com/cache/550x550/ts/product/70/a6/3a/2e2d6dfa44b8d49c4c312d876054b66d.jpg',
                    smallImg: 'https://salt.tikicdn.com/cache/75x75/ts/product/70/a6/3a/2e2d6dfa44b8d49c4c312d876054b66d.jpg'
                },
                {
                    id: '1',
                    nameProduct: 'Tên sản phẩm',
                    price: 1800000,
                    bigImg: 'https://salt.tikicdn.com/cache/550x550/ts/product/a5/b1/03/60de1319622d3775ed78fa2199dcb317.jpg',
                    smallImg: 'https://salt.tikicdn.com/cache/75x75/ts/product/a5/b1/03/60de1319622d3775ed78fa2199dcb317.jpg'
                },
                {
                    id: '2',
                    nameProduct: 'Tên sản phẩm',
                    price: 1800000,
                    promotion: 10,
                    bigImg: 'https://salt.tikicdn.com/cache/550x550/ts/product/98/0a/a6/9d9d1e30d026b166a0e90e83010f81d9.jpg',
                    smallImg: 'https://salt.tikicdn.com/cache/75x75/ts/product/98/0a/a6/9d9d1e30d026b166a0e90e83010f81d9.jpg'
                },
                {
                    id: '3',
                    nameProduct: 'Tên sản phẩm',
                    price: 1800000,
                    bigImg: 'https://salt.tikicdn.com/cache/550x550/ts/product/82/71/e8/e733b686fb297f235ac2c86482fa3547.jpg',
                    smallImg: 'https://salt.tikicdn.com/cache/75x75/ts/product/82/71/e8/e733b686fb297f235ac2c86482fa3547.jpg'
                },
                {
                    id: '4',
                    nameProduct: 'Tên sản phẩm',
                    price: 1800000,
                    bigImg: 'https://salt.tikicdn.com/cache/550x550/ts/product/70/a6/3a/2e2d6dfa44b8d49c4c312d876054b66d.jpg',
                    smallImg: 'https://salt.tikicdn.com/cache/75x75/ts/product/70/a6/3a/2e2d6dfa44b8d49c4c312d876054b66d.jpg'
                },
            ]
        };
    }

    async componentDidMount() {
        const { Detail } = this.props;
        if (Detail.detail) {
            const detail = await HomeService.getDetailProduct(Detail.detail.pid);
            console.log('DaTa', JSON.stringify(detail));
            
            if (detail) {
                this.setState({
                    data: detail
                })
            }
        }
    }

    // onClickItem = (index) => {
    //     this.setState({
    //         currentItem: index
    //     })
    // }

    onClickIncrease = () => {
        this.setState({
            value: this.state.value + 1
        })
    }
    onClickDecrease = () => {
        if (this.state.value >= 1) {
            this.setState({
                value: this.state.value - 1
            })
        }
    }

    handleAddCart = () => {
        const { Cart, Detail } = this.props;
        const item = {
            amount: this.state.value,
            product: this.state.data,
            image: `http://localhost:9999/images/${Detail.detail.imageSource}`
        }
        Cart.items.push(item);
        alert('Thêm vào giỏ hàng thành công');
    }

    renderProduct() {
        const { Detail } = this.props
        const { currentItem, data, value } = this.state;
        return (
            <div style={{ width: '100%', height: '75vh' }}>
                <div style={{ display: 'flex', flexDirection: 'row', paddingTop: 20 }}>
                    <div style={{ display: 'flex', width: '40%', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                        {
                            Detail.detail.imageSource ? 
                                <img src={`http://localhost:9999/images/${Detail.detail.imageSource}`} alt='img' width={400} height={450} />
                                : null
                        }
                        {/* <div style={{ display: 'flex', paddingTop: 20 }}>
                            {
                                data.img.map((item, index) => {
                                    return (
                                        <div key={index.toString()}
                                            className={index === currentItem ? 'containerSmallImgPicked' : 'containerSmallImg'}
                                        >
                                            <Button ghost type='link' onClick={() => this.onClickItem(index)}>
                                                <img src={item.smallImg} alt='img' />
                                            </Button>
                                        </div>
                                    )
                                })
                            }
                        </div> */}
                    </div>
                    <div style={{ display: 'flex', width: '30%', alignItems: 'center', flexDirection: 'column', paddingTop: 60 }}>
                        <div style={{ color: '#1790FF', fontSize: 15 }}>{data.pname}</div>
                        <div>{`${formatMoney(data.price.toString())}đ`}</div>
                        <div>
                            <Input
                                style={{ textAlign: 'center', padding: 12 }}
                                value={value}
                                suffix={<Button onClick={this.onClickIncrease}>+</Button>}
                                prefix={<Button onClick={this.onClickDecrease}>-</Button>} />
                        </div>
                        <div style={{ paddingTop: 30 }}>
                            <Button style={{ borderColor: 'green' }} onClick={this.handleAddCart}>
                                Thêm vào giỏ hàng
                            </Button>
                        </div>
                    </div>
                    <div style={{ width: '30%', display: 'flex', flexDirection: 'column', paddingTop: 60 }}>
                        <div style={{ color: '#000000', fontSize: 30, textAlign: 'center' }}>{'Thông số chi tiết'}</div>
                        <div>
                            {/* <div className='txtDetail'>{data.parameter.screen}</div>
                            <div className='txtDetail'>{data.parameter.os}</div>
                            <div className='txtDetail'>{data.parameter.chip}</div>
                            <div className='txtDetail'>{data.parameter.ram}</div>
                            <div className='txtDetail'>{data.parameter.memory}</div>
                            <div className='txtDetail'>{data.parameter.backCamera}</div>
                            <div className='txtDetail'>{data.parameter.frontCamera}</div> */}
                            <div className='txtDetail'>{data.info.replace(/\\n/g,"<br />")}</div>
                            <div className='txtDetail'>{data.description}</div>
                        </div>
                    </div>()
                </div>
            </div>
        );
    }
    render() {
        const { otherProduct, data } = this.state;
        return (
            <div>
                <BaseContentCustomer>
                   {data ? this.renderProduct() : null} 
                    <div style={{ width: '100%' }}>
                        <div style={{ textAlign: 'center', color: '#000000', fontSize: 20 }}>Sản phẩm liên quan</div>
                        <div style={{ display: 'flex', paddingLeft: 10, paddingRight: 5 }}>
                            {
                                otherProduct.map((item, index) => {
                                    return (
                                        <div style={{ flex: 1, height: 100, borderStyle: 'solid', borderWidth: 1, borderColor: 'black', marginLeft: 5, marginRight: 5, alignItems: 'center', justifyContent: 'flex-start', display: 'flex' }}>
                                            <img src={item.smallImg} alt='img' />
                                            <div style={{ display: 'flex', flexDirection: 'column', flexWrap: 'wrap' }}>
                                                <div>{item.nameProduct}</div>
                                                <div>{formatMoney(item.price.toString())}</div>
                                                {
                                                    item && item.promotion &&
                                                    <div style={{ textAlign: 'center', fontWeight: 'bold' }}>{`-${item.promotion}%`}</div>
                                                }
                                            </div>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </div>
                </BaseContentCustomer>
            </div >
        );
    }
}

export default index;
