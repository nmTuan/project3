import React, { Component } from 'react';
import Home from './Home';
import { inject, observer } from 'mobx-react';
import ManageProduct from './ManageProduct';

@inject('AuthenStore')
@observer
class Test extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const { AuthenStore } = this.props;
        return (
            <div>
                {
                    AuthenStore.isAdmin ?
                        <ManageProduct />
                        :
                        <Home history={this.props.history} />
                }
            </div>
        );
    }
}

export default Test;
