import React, { Component } from 'react';
import { Col, Input, Button } from 'antd';
import BaseContentCustomer from '../BaseContentCustomer.js';
import { inject, observer } from 'mobx-react';

const styles = {
    styleInput: { width: '40%', paddingTop: 10 },
    container: { marginTop: 20, width: '100%', display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' },
    styleTitle: { width: '40%', display: 'flex', justifyContent: 'flex-start' }
}

@inject('AuthenStore')
@observer
class UpdateUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    onCLickBack = () => {
        const { history } = this.props;
        history.push('/')
    }

    render() {
        const { AuthenStore } = this.props;
        return (
            <div>
                <BaseContentCustomer>
                    <div style={{ display: 'flex', justifyContent: 'center', marginTop: 30 }}>
                        <h1>Thông tin cá nhân</h1>
                    </div>
                    <Col offset={4} span={16}>
                        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                            <div style={styles.container}>
                                <div style={styles.styleTitle}>Họ và tên</div>
                                <div style={styles.styleInput}>
                                    <Input
                                        placeholder='Họ và tên'
                                        defaultValue={AuthenStore.userInfor && AuthenStore.userInfor.name ? AuthenStore.userInfor.name : ''}
                                    />
                                </div>
                            </div>
                            <div style={styles.container}>
                                <div style={styles.styleTitle}>Email</div>
                                <div style={styles.styleInput}>
                                    <Input
                                        placeholder='Email'
                                        defaultValue={AuthenStore.userInfor && AuthenStore.userInfor.email ? AuthenStore.userInfor.email : ''}
                                    />
                                </div>
                            </div>
                            <div style={styles.container}>
                                <div style={styles.styleTitle}>Số điện thoại</div>
                                <div style={styles.styleInput}>
                                    <Input
                                        placeholder='Số điện thoại'
                                        defaultValue={AuthenStore.userInfor && AuthenStore.userInfor.phone ? AuthenStore.userInfor.phone : ''}
                                    />
                                </div>
                            </div>
                            <div style={styles.container}>
                                <div style={styles.styleTitle}>Địa chỉ</div>
                                <div style={styles.styleInput}>
                                    <Input
                                        placeholder='Địa chỉ'
                                        defaultValue={AuthenStore.userInfor && AuthenStore.userInfor.address ? AuthenStore.userInfor.address : ''}
                                    />
                                </div>
                            </div>
                            <div style={{ marginTop: 40 }}>
                                <Button onClick={this.onCLickBack}>Quay lại</Button>
                                <Button type='primary' style={{ marginLeft: 30 }}>Cập nhật</Button>
                            </div>
                        </div>
                    </Col>
                </BaseContentCustomer>

            </div>
        );
    }
}

export default UpdateUser;
