import React, { Component } from 'react';
import { Carousel, Col, Row, Button, Modal, Switch } from "antd";
import { inject, observer } from 'mobx-react';
import { Link } from 'react-router-dom';

import './Home.css'
import Login from '../Login/index';
import Register from '../Register';
import { HomeService } from '../../services/HomeService';
import Config from '../../config/Config';
import BaseContentCustomer from '../BaseContentCustomer.js';

@inject('Header', 'Detail')
@observer
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listNewProduct: [],
            listPhone: [],
            listLaptop: [],
            listTablet: [],
            listDevices: [],

        };
    }

    async componentDidMount() {
        const newProduct = await HomeService.getNewProduct();
        if (newProduct) {
            this.setState({
                listNewProduct: newProduct
            })
        }
        const listPhone = await HomeService.getProductForHome('PHONE', 0, 5);
        if (listPhone) {
            this.setState({
                listPhone
            })
        }
        const listLaptop = await HomeService.getProductForHome('LAPTOP', 0, 5);
        if (listLaptop) {
            this.setState({
                listLaptop
            })
        }
        const listTablet = await HomeService.getProductForHome('TABLET', 0, 5);
        if (listTablet) {
            this.setState({
                listTablet
            })
        }
        const listDevices = await HomeService.getProductForHome('DEVICE', 0, 5);
        if (listDevices) {
            this.setState({
                listDevices
            })
        }

    }

    dissmissLogin = () => {
        const { Header } = this.props;
        Header.showModalLogin = false;
    }

    dissmissRegister = () => {
        const { Header } = this.props;
        Header.showModalRegister = false;
    }

    onClickCart = () => {
        // alert('asdf')
    }

    onClickTypeProduct = (type) => {
        const { Header, history } = this.props;
        const typeScreen = Config.typeScreen;
        Header.setTypeScreen(type);
        switch (type) {
            case typeScreen.phone:
                history.push('/phone');
                break;
            case typeScreen.laptop:
                history.push('/laptop');
                break;
            case typeScreen.tablet:
                history.push('/tablet');
                break;
            case typeScreen.accessories:
                history.push('/device');
                break;
            default:
                break;
        }
    }

    onClickItem = (item) => {
        const { Detail } = this.props;
        Detail.setItem(item);

    }

    renderNewProduct() {
        return (
            <div className='containerItem'>
                <div className='backgroundNewProduct'>
                    <h2 style={{ color: '#ffffff' }}>Sản phẩm mới</h2>
                </div>
                <Row type='flex' justify='space-between' align='middle'>
                    {
                        this.state.listNewProduct.map((item, index) => {
                            return (
                                <Col span={3}>
                                    <div className='borderItem'>
                                        <Link to='/Detail' onClick={() => this.onClickItem(item)}>
                                            <div>
                                                <img src={`http://localhost:9999/images/${item.imageSource}`} alt='banner' height={120} width={120} />
                                            </div>
                                            <div style={{ paddingTop: 20 }}>
                                                <div style={{ color: '#61dded' }}>{item.pname}</div>
                                                <div >
                                                    <div>{`${item.price}VNĐ`}</div>
                                                    <div>{item.discount > 0 ? `-${item.discount}%` : ''}</div>
                                                </div>
                                            </div>
                                        </Link>
                                        <div style={{ paddingTop: item.discount > 0 ? 4 : 22, paddingBottom: 10 }}>
                                            <Button className='btnAdd2Card' onClick={this.onClickCart}>
                                                Thêm vào giỏ hàng
                                            </Button>
                                        </div>
                                    </div>
                                </Col>
                            );
                        })
                    }
                </Row>
            </div>
        );
    }

    renderLaptop() {
        return (
            <div className='containerItem'>
                <div className='backgroundProduct'>
                    <h2 style={{ color: '#ffffff' }}>Laptop</h2>
                </div>
                <Row type='flex' justify='space-between' align='middle'>
                    {
                        this.state.listLaptop.map((item, index) => {
                            return (
                                <Col span={3}>
                                    <div className='borderItem'>
                                        <Link to='/Detail' onClick={() => this.onClickItem(item)}>
                                            <div>
                                                <img src={`http://localhost:9999/images/${item.imageSource}`} alt='banner' height={120} width={120} />
                                            </div>
                                            <div style={{ paddingTop: 20 }}>
                                                <div style={{ color: '#61dded' }}>{item.pname}</div>
                                                <div >
                                                    <div>{`${item.price}VNĐ`}</div>
                                                    <div>{item.discount > 0 ? `-${item.discount}%` : ''}</div>
                                                </div>
                                            </div>
                                        </Link>
                                        <div style={{ paddingTop: item.discount > 0 ? 4 : 22, paddingBottom: 10 }}>
                                            <Button className='btnAdd2Card'>
                                                Thêm vào giỏ hàng
                                            </Button>
                                        </div>
                                    </div>
                                </Col>
                            );
                        })
                    }
                </Row>
                <div style={{ textAlign: 'center', paddingTop: 25 }}>
                    <Button style={{ backgroundColor: '#33f260' }} onClick={() => this.onClickTypeProduct(Config.typeScreen.laptop)}>
                        Xem thêm
                  </Button>
                </div>
            </div>
        );
    }

    renderPhone() {
        return (
            <div className='containerItem'>
                <div className='backgroundProduct'>
                    <h2 style={{ color: '#ffffff' }}>Điện thoại</h2>
                </div>
                <Row type='flex' justify='space-between' align='middle'>
                    {
                        this.state.listPhone.map((item, index) => {
                            return (
                                <Col span={3}>
                                    <div className='borderItem'>
                                        <Link to='/Detail' onClick={() => this.onClickItem(item)}>
                                            <div>
                                                <img src={`http://localhost:9999/images/${item.imageSource}`} alt='banner' height={120} width={120} />
                                            </div>
                                            <div style={{ paddingTop: 20 }}>
                                                <div style={{ color: '#61dded' }}>{item.pname}</div>
                                                <div >
                                                    <div>{`${item.price}VNĐ`}</div>
                                                    <div>{item.discount > 0 ? `-${item.discount}%` : ''}</div>
                                                </div>
                                            </div>
                                        </Link>
                                        <div style={{ paddingTop: item.discount > 0 ? 4 : 22, paddingBottom: 10 }}>
                                            <Button className='btnAdd2Card'>
                                                Thêm vào giỏ hàng
                                            </Button>
                                        </div>
                                    </div>
                                </Col>
                            );
                        })
                    }
                </Row>
                <div style={{ textAlign: 'center', paddingTop: 25 }}>
                    <Button style={{ backgroundColor: '#33f260' }} onClick={() => this.onClickTypeProduct(Config.typeScreen.phone)}>
                        Xem thêm
                  </Button>
                </div>
            </div>
        );
    }

    renderTablet() {
        return (
            <div className='containerItem'>
                <div className='backgroundProduct'>
                    <h2 style={{ color: '#ffffff' }}>Tablet</h2>
                </div>
                <Row type='flex' justify='space-between' align='middle'>
                    {
                        this.state.listTablet.map((item, index) => {
                            return (
                                <Col span={3}>
                                    <div className='borderItem'>
                                        <Link to='/Detail' onClick={() => this.onClickItem(item)}>
                                            <div>
                                                <img src={`http://localhost:9999/images/${item.imageSource}`} alt='banner' height={120} width={120} />
                                            </div>
                                            <div style={{ paddingTop: 20 }}>
                                                <div style={{ color: '#61dded' }}>{item.pname}</div>
                                                <div >
                                                    <div>{`${item.price}VNĐ`}</div>
                                                    <div>{item.discount > 0 ? `-${item.discount}%` : ''}</div>
                                                </div>
                                            </div>
                                        </Link>
                                        <div style={{ paddingTop: item.discount > 0 ? 4 : 22, paddingBottom: 10 }}>
                                            <Button className='btnAdd2Card'>
                                                <div>Thêm vào giỏ hàng</div>
                                            </Button>
                                        </div>
                                    </div>
                                </Col>
                            );
                        })
                    }
                </Row>
                <div style={{ textAlign: 'center', paddingTop: 25 }}>
                    <Button style={{ backgroundColor: '#33f260' }} onClick={() => this.onClickTypeProduct(Config.typeScreen.tablet)}>
                        Xem thêm
                  </Button>
                </div>
            </div>
        );
    }

    renderAccessories() {
        return (
            <div className='containerItem'>
                <div className='backgroundProduct'>
                    <h2 style={{ color: '#ffffff' }}>Phụ kiện</h2>
                </div>
                <Row type='flex' justify='space-between' align='middle'>
                    {
                        this.state.listDevices.map((item, index) => {
                            return (
                                <Col span={3}>
                                    <div className='borderItem'>
                                        <Link to='/Detail' onClick={() => this.onClickItem(item)}>
                                            <div>
                                                <img src={`http://localhost:9999/images/${item.imageSource}`} alt='banner' height={120} width={120} />
                                            </div>
                                            <div style={{ paddingTop: 20 }}>
                                                <div style={{ color: '#61dded' }}>{item.pname}</div>
                                                <div >
                                                    <div>{`${item.price}VNĐ`}</div>
                                                    <div>{item.discount > 0 ? `-${item.discount}%` : ''}</div>
                                                </div>
                                            </div>
                                        </Link>
                                        <div style={{ paddingTop: item.discount > 0 ? 4 : 22, paddingBottom: 10 }}>
                                            <Button className='btnAdd2Card'>
                                                Thêm vào giỏ hàng
                                            </Button>
                                        </div>
                                    </div>
                                </Col>
                            );
                        })
                    }
                </Row>
                <div style={{ textAlign: 'center', paddingTop: 25 }}>
                    <Button style={{ backgroundColor: '#33f260' }} onClick={() => this.onClickTypeProduct(Config.typeScreen.accessories)}>
                        Xem thêm
                  </Button>
                </div>
            </div>
        );
    }

    render() {
        const { Header } = this.props;
        return (
            <div>
                <BaseContentCustomer>
                    <div style={{ flex: 1, paddingTop: 5, paddingLeft: 10, paddingRight: 10 }}>
                        <Row>
                            <Col span={14}>
                                <Carousel autoplay dotPosition='bottom'>
                                    <div>
                                        <div className='container' style={{ backgroundImage: 'url(http://laptop88.vn/media/banner/19_Sep79b5126004424d7332f334f4e14bd7f1.jpg)' }} />
                                    </div>
                                    <div>
                                        <div className='container' style={{ backgroundImage: 'url(http://laptop88.vn/media/banner/10_Oct8a13fe05f1e4fedf0b068118f630f6be.png)' }} />
                                    </div>
                                    <div>
                                        <div className='container' style={{ backgroundImage: 'url(http://laptop88.vn/media/banner/10_Oct8a13fe05f1e4fedf0b068118f630f6be.png)' }} />
                                    </div>
                                    <div>
                                        <div className='container' style={{ backgroundImage: 'url(http://laptop88.vn/media/banner/10_Oct8a13fe05f1e4fedf0b068118f630f6be.png)' }} />
                                    </div>
                                </Carousel>
                            </Col>
                            <Col span={10} >
                                <div>
                                    <div>
                                        <div className='background' style={{ backgroundImage: 'url(http://laptop88.vn/media/banner/02_Mar3c734b332977546eb59cbafb88bd03dd.png)' }} />
                                    </div>
                                    <div>
                                        <div className='background' style={{ backgroundImage: 'url(http://laptop88.vn/media/banner/02_Mar79cb4f24ede70a27130be13e35a4a419.png)' }} />
                                    </div>
                                </div>
                            </Col>
                        </Row>
                        <div>
                            <img src='http://laptop88.vn/media/banner/25_Jul91bec3f522355baf5dde1453f3c3e931.png' alt='banner' width='100%' />
                        </div>
                        {this.renderNewProduct()}
                        {this.renderLaptop()}
                        {this.renderPhone()}
                        {this.renderTablet()}
                        {this.renderAccessories()}
                        <Modal visible={Header.showModalLogin} footer={null} onCancel={this.dissmissLogin} maskClosable>
                            <div>
                                <Login history={this.props.history} />
                            </div>
                        </Modal>
                        <Modal visible={Header.showModalRegister} footer={null} onCancel={this.dissmissRegister} maskClosable>
                            <div>
                                <Register />
                            </div>
                        </Modal>
                    </div>
                </BaseContentCustomer>
            </div>
        );
    }
}

export default Home;
