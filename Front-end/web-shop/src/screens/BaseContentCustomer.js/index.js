import React, { Component } from 'react';
import Header from '../../layout/header';

class BaseContentCustomer extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <div>
                <Header />
                {this.props.children}
            </div>
        );
    }
}

export default BaseContentCustomer;
