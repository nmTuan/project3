//Name. Email. Password. Phone. Address
import React, { Component } from 'react';
import { Input, Icon, Button } from 'antd';
import { Authentication } from '../../services/Authentication';
import { inject, observer } from 'mobx-react';

@inject('Header')
@observer
class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fullname: '',
            username: '',
            email: '',
            password: '',
            rePassword: '',
            phone: '',
            address: ''
        };
    }

    onChange = (key, event) => {
        switch (key) {
            case 0:
                this.setState({
                    fullname: event.target.value
                })
                break;
            case 1:
                this.setState({
                    email: event.target.value
                })
                break;
            case 2:
                this.setState({
                    password: event.target.value
                })
                break;
            case 3:
                this.setState({
                    rePassword: event.target.value
                })
                break;
            case 4:
                this.setState({
                    phone: event.target.value
                })
                break;
            case 5:
                this.setState({
                    address: event.target.value
                })
                break;
            default:
                break;
        }
    }

    onClickLogin = async () => {
        const { Header } = this.props;
        const { fullname, password, email, rePassword, phone, address } = this.state;
        if (fullname !== '' && password !== '' && email !== '' && rePassword !== '' && phone !== '' && address !== '') {
            if(password === rePassword){
                const res = await Authentication.singup(fullname, email, password, phone, address)
                if(res.success){
                    Header.showModalRegister = false;
                    alert('Đăng ký thành công');
                } else {
                    alert(res.message)
                }
            } else {
                alert('Mật khẩu không trùng khớp')

            }
        } else {
            alert('Vui lòng điền đầy đủ thông tin')
        }
    }

    render() {
        const { fullname, email, password, rePassword, phone, address } = this.state;
        return (
            <div>
                <div style={{ padding: 15 }}>
                    <div style={{ color: '#000000', fontSize: 24, fontWeight: 'bold' }}>Đăng ký</div>
                    <div style={{ paddingTop: 15 }}>
                        <div >
                            <Input
                                prefix={
                                    <div style={{ maxHeight: '100%', width: 40, margin: 0, paddingBottom: 0 }}>
                                        <Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />
                                    </div>}
                                placeholder='Họ và tên *'
                                value={fullname}
                                onChange={event => this.onChange(0, event)}
                            />
                        </div>
                        <div style={{ paddingTop: 8 }}>
                            <Input
                                prefix={
                                    <div style={{ maxHeight: '100%', width: 40, margin: 0, paddingBottom: 0 }}>
                                        <Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />
                                    </div>}
                                placeholder='Email *'
                                type='email'
                                value={email}
                                onChange={event => this.onChange(1, event)}
                            />
                        </div>
                        <div style={{ paddingTop: 8 }}>
                            <Input
                                prefix={
                                    <div style={{ maxHeight: '100%', width: 40, margin: 0, paddingBottom: 0 }}>
                                        <Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />
                                    </div>}
                                placeholder='Password *'
                                value={password}
                                type='password'
                                onChange={event => this.onChange(2, event)}
                            />
                        </div>
                        <div style={{ paddingTop: 8 }}>
                            <Input
                                prefix={
                                    <div style={{ maxHeight: '100%', width: 40, margin: 0, paddingBottom: 0 }}>
                                        <Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />
                                    </div>}
                                placeholder='Re-password *'
                                value={rePassword}
                                type='password'
                                onChange={event => this.onChange(3, event)}
                            />
                        </div>
                        <div style={{ paddingTop: 8 }}>
                            <Input
                                prefix={
                                    <div style={{ maxHeight: '100%', width: 40, margin: 0, paddingBottom: 0 }}>
                                        <Icon type="phone" style={{ color: 'rgba(0,0,0,.25)' }} />
                                    </div>}
                                placeholder='Số điện thoại *'
                                value={phone}
                                onChange={event => this.onChange(4, event)}
                            />
                        </div>
                        <div style={{ paddingTop: 8 }}>
                            <Input
                                prefix={
                                    <div style={{ maxHeight: '100%', width: 40, margin: 0, paddingBottom: 0 }}>
                                        <Icon type="home" style={{ color: 'rgba(0,0,0,.25)' }} />
                                    </div>}
                                placeholder='Địa chỉ'
                                value={address}
                                onChange={event => this.onChange(5, event)}
                            />
                        </div>
                    </div>
                    <div style={{ width: '100%', display: 'flex', justifyContent: 'center', paddingTop: 10 }}>
                        <Button
                            style={{ backgroundColor: '#0cf22f', borderColor: '#0cf22f' }}
                            onClick={this.onClickLogin}
                        >Đăng Ký</Button>
                    </div>
                </div>
            </div>
        );
    }
}

export default Register;
