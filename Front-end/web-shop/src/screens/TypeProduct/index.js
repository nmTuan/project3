import React, { Component } from 'react';
import { Col, Pagination, Button } from 'antd';
import { inject, observer } from 'mobx-react';

import './styles.css';
import BaseContentCustomer from '../BaseContentCustomer.js';
import Config from '../../config/Config';
import { HomeService } from '../../services/HomeService';

@inject('Header')
@observer
class TypeProduct extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    handleChangePage = (page, pageSize) => {
        this.props.handleChangePage(page, pageSize);
    }

    onClickItem = (item) => {
        this.props.onClickItem(item)
    }

    render() {
        const { Header, listCurrentProduct, sumProduct } = this.props;
        return (
            <div>
                <BaseContentCustomer>
                    <Col offset={4} span={16}>
                        <div className='backgroundNewProduct'>
                            <h2 style={{ color: '#ffffff' }}>{Header.typeScreen}</h2>
                        </div>
                        <div style={{ display: 'flex', width: '100%', flexWrap: 'wrap' }}>
                            {listCurrentProduct.map((item, index) => {
                                return (
                                    <Col span={6}>
                                        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', margin: 5, height: 250, borderStyle: 'solid', borderWidth: 1, borderRadius: 3, borderColor: '#e6f5ea' }}>
                                            <Button type='link' onClick={() => this.onClickItem(item)}>
                                                <div style={{ width: 150, height: 150, backgroundColor: 'red', marginTop: 10 }}>
                                                    {
                                                        item.imageSource &&
                                                        <img src={`http://localhost:9999/images/${item.imageSource}`} alt='banner' height={150} width={150} />
                                                    }
                                                </div>
                                                <div style={{width:200, textOverflow: 'ellipsis', overflow: 'hidden', whiteSpace: 'nowrap' }}>{item.pname && item.pname}</div>
                                                <div>{item.discount && `-${item.discount}%`}</div>
                                                <div>{item.price && item.price}</div>
                                            </Button>
                                        </div>
                                    </Col>
                                );
                            })}
                        </div>
                        <div style={{ width: '100%', display: 'flex', justifyContent: 'flex-end', paddingTop: 20, paddingBottom: 30 }}>
                            <Pagination defaultCurrent={1} pageSize={20} total={sumProduct} onChange={this.handleChangePage} />
                        </div>
                    </Col>
                </BaseContentCustomer>
            </div>
        );
    }
}

export default TypeProduct;
