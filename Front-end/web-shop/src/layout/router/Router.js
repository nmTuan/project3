import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from '../../screens/Home';
import Detail from '../../screens/Detail';
import Cart from "../../screens/Cart";
import Buy from '../../screens/Buy';
import ManageProduct from '../../screens/ManageProduct';
import Path from './Path';
import TypeProduct from '../../screens/TypeProduct';
import AddProduct from '../../screens/AddProduct';
import Phone from '../../screens/Phone';
import Laptop from '../../screens/Laptop';
import Tablet from '../../screens/Tablet';
import Device from '../../screens/Device';
import UpdateUser from '../../screens/UpdateUser';

class AppRouter extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <Switch>
                <Route exact path='/' component={Home} />
                <Route exact path='/detail' component={Detail} />
                <Route exact path='/cart' component={Cart} />
                <Route exact path='/buy' component={Buy} />
                <Route exact path='/product' component={TypeProduct} />
                <Route exact path='/phone' component={Phone} />
                <Route exact path='/laptop' component={Laptop} />
                <Route exact path='/tablet' component={Tablet} />
                <Route exact path='/device' component={Device} />
                <Path exact path='/manager' component={ManageProduct} />
                <Path exact path='/addProduct' component={AddProduct} />
                <Path exact path='/userInfor' component={UpdateUser} />
            </Switch>
        );
    }
}

export default AppRouter;
