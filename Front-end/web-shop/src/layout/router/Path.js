import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { inject, observer } from 'mobx-react';

@inject('AuthenStore')
@observer
class Path extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const { path, component, AuthenStore } = this.props;
        if (path === '/manager' || path === '/addProduct') {
            if (AuthenStore.isAdmin) return <Route exact component={component} />
            else return <Redirect to='/' />
        } else if (path === '/userInfor') {
            if (AuthenStore.isLogin) return <Route exact component={component} />
            else return <Redirect to='/' />
        } else {
            return <Route exact component={component} />
        }
    }
}

export default Path;
