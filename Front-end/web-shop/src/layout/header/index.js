import React, { Component } from 'react';
import { Row, Col, Button, Dropdown, Menu } from 'antd';
import Search from 'antd/lib/input/Search';
import { inject, observer } from 'mobx-react';
import { Link } from 'react-router-dom';

import logo from '../../assets/logo.png';
import smartphone from '../../assets/smartphone.png';
import device from '../../assets/device.png';
import laptop from '../../assets/laptop.png';
import tablet from '../../assets/tablet.png';
import cart from '../../assets/cart.png';
import Config from '../../config/Config';

const styles = {
    backgroundSelected: { backgroundColor: 'transparent', borderColor: '#3E94D5' },
    normalBackground: { backgroundColor: 'transparent', borderColor: 'transparent' },
    textSelected: { color: '#3E94D5' },
    normalText: { color: '#000000' },
}
@inject('Header', 'AuthenStore')
@observer
class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    onSearch = (value) => {
        console.log(value)
    }

    handleLogin = () => {
        const { Header } = this.props;
        Header.showModalLogin = true;
    }

    handleRegister = () => {
        const { Header } = this.props;
        Header.showModalRegister = true;
    }

    onClickTypeProduct = (type) => {
        const { Header } = this.props;
        Header.setTypeScreen(type);
    }

    onClickLogin = () => {
        const { AuthenStore } = this.props;
        AuthenStore.isLogin = false;
    }

    render() {
        const { Header, AuthenStore } = this.props;
        const typeScreen = Config.typeScreen;
        const menuCustomer = (
            <Menu>
                <Menu.Item key='0'>
                    <Button style={{ width: '100%' }}>
                        <Link to='/cart'>
                            Quản lý giỏ hàng
                        </Link>
                    </Button>
                </Menu.Item>
                <Menu.Item key='1'>
                    <Button style={{ width: '100%' }}>
                        <Link to='/userInfor'>
                            Thông tin cá nhân
                        </Link>
                    </Button>
                </Menu.Item>
                <Menu.Item key='2'>
                    <Button style={{ width: '100%' }} onClick={this.onClickLogin}>Đăng xuất</Button>
                </Menu.Item>
            </Menu>
        );
        return (
            <div style={{ width: '100%', height: 100, backgroundColor: '#03ecfc80', alignItems: 'center' }}>
                <Row type='flex' align='middle' justify='space-between' style={{ height: '100%' }}>
                    <Col>
                        <Link to='/'>
                            <img src={logo} alt="" style={{ width: 60, height: 40 }} />
                        </Link>
                    </Col>
                    <Col span={8}>
                        <Search
                            placeholder="Search..."
                            enterButton="Search"
                            onSearch={this.onSearch}
                        />
                    </Col>
                    <Col span={9}>
                        <Row type='flex' justify='space-between'>
                            <Col>
                                <Button onClick={() => this.onClickTypeProduct(typeScreen.phone)}
                                    style={Header.typeScreen === typeScreen.phone ? styles.backgroundSelected : styles.normalBackground}
                                >
                                    <Link to='/phone'>
                                        <img src={smartphone} alt="" style={{ width: 30, height: 30 }} />
                                        <div style={Header.typeScreen === typeScreen.phone ? styles.textSelected : styles.normalText}>Điện thoại</div>
                                    </Link>
                                </Button>

                            </Col>
                            <Col>
                                <Button onClick={() => this.onClickTypeProduct(typeScreen.tablet)}
                                    style={Header.typeScreen === typeScreen.tablet ? styles.backgroundSelected : styles.normalBackground}
                                >
                                    <Link to='/tablet'>
                                        <img src={tablet} alt="" style={{ width: 30, height: 30 }} />
                                        <div style={Header.typeScreen === typeScreen.tablet ? styles.textSelected : styles.normalText}>Tablet/Ipad</div>
                                    </Link>
                                </Button>
                            </Col>
                            <Col>
                                <Button onClick={() => this.onClickTypeProduct(typeScreen.laptop)}
                                    style={Header.typeScreen === typeScreen.laptop ? styles.backgroundSelected : styles.normalBackground}
                                >
                                    <Link to='/laptop'>
                                        <img src={laptop} alt="" style={{ width: 30, height: 30 }} />
                                        <div style={Header.typeScreen === typeScreen.laptop ? styles.textSelected : styles.normalText}>Laptop</div>
                                    </Link>
                                </Button>
                            </Col>
                            <Col>
                                <Button onClick={() => this.onClickTypeProduct(typeScreen.accessories)}
                                    style={Header.typeScreen === typeScreen.accessories ? styles.backgroundSelected : styles.normalBackground}
                                >
                                    <Link to='/device'>
                                        <img src={device} alt="" style={{ width: 30, height: 30 }} />
                                        <div style={Header.typeScreen === typeScreen.accessories ? styles.textSelected : styles.normalText}>Phụ kiện</div>
                                    </Link>
                                </Button>
                            </Col>
                            <Col>
                                <Link to='/cart'>
                                    <Button style={{ backgroundColor: 'transparent', borderColor: 'transparent' }}>
                                        <img src={cart} alt="" style={{ width: 30, height: 30 }} />
                                        <div>Giỏ hàng</div>
                                    </Button>
                                </Link>

                            </Col>
                        </Row>
                    </Col>
                    <Col span={5} style={{ paddingRight: 25, paddingLeft: 25 }}>
                        {
                            AuthenStore.isLogin ?
                                <Row type='flex' justify='end'>
                                    <Dropdown overlay={menuCustomer} trigger={['click']}>
                                        <Button type='link' onClick={() => this.props.AuthenStore.setRole(false)}>
                                            {AuthenStore.userInfor && AuthenStore.userInfor.name ? AuthenStore.userInfor.name : 'Khách '}
                                        </Button>
                                    </Dropdown>
                                </Row>
                                :
                                <Row type="flex" justify='space-between'>
                                    <Col>
                                        <Button type="primary" onClick={this.handleLogin}>Đăng nhập</Button>
                                    </Col>
                                    <Col>
                                        <Button onClick={this.handleRegister}>Đăng ký</Button>
                                    </Col>
                                </Row>
                        }
                    </Col>
                </Row>
            </div >
        );
    }
}

export default Header;
