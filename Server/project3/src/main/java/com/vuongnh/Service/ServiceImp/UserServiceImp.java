package com.vuongnh.Service.ServiceImp;

import com.vuongnh.Service.UserService;
import com.vuongnh.domain.User;
import com.vuongnh.payload.request.UpdateUserDetailsRequest;
import com.vuongnh.payload.response.UserDetails;
import com.vuongnh.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImp implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public UserDetails getUserInfo(Long id) {

        Optional<User> user = userRepository.findById(id);

        UserDetails userDetails = new UserDetails();

        userDetails.setName(user.get().getName());
        userDetails.setEmail(user.get().getEmail());
        userDetails.setPhone(user.get().getPhone());
        userDetails.setAddress(user.get().getAddress());

        return userDetails;
    }

    @Override
    public void updateUserInfo(UpdateUserDetailsRequest updateUserDetailsRequest, Long uid) {

        String name= updateUserDetailsRequest.getName();

        String email = updateUserDetailsRequest.getEmail();

        String password = passwordEncoder.encode(updateUserDetailsRequest.getPassword());

        String phone = updateUserDetailsRequest.getPhone();

        String address = updateUserDetailsRequest.getAddress();


        userRepository.updateUserInfo(name,email,password,phone,address,uid);

    }
}
