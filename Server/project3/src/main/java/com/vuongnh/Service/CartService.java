package com.vuongnh.Service;

import com.vuongnh.payload.request.CartVerifyRequest;
import com.vuongnh.payload.response.CartVerifyResponse;

import java.util.List;

public interface CartService {

    List<CartVerifyResponse> verifyCart(List<CartVerifyRequest> cartVerifyRequests);

}
