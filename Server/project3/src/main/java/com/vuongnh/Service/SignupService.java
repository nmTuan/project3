package com.vuongnh.Service;

import com.vuongnh.payload.request.SignUpRequest;
import org.springframework.http.ResponseEntity;

public interface SignupService {

    ResponseEntity<?> signUp(SignUpRequest signUpRequest);

}
