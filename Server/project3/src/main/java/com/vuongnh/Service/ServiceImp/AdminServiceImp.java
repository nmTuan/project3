package com.vuongnh.Service.ServiceImp;

import com.vuongnh.Service.AdminService;
import com.vuongnh.payload.response.AllProductForAdmin;
import com.vuongnh.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AdminServiceImp implements AdminService {

    @Autowired
    ProductRepository productRepository;

    @Override
    public List<AllProductForAdmin> getAllProductForAdmin(int pageIndex, int sizeOfPage) {

        Pageable pageable = PageRequest.of(pageIndex,sizeOfPage);

        Page<AllProductForAdmin> allProductForAdmins = productRepository.getAllProduct(pageable);

        List<AllProductForAdmin> allProductForAdmins1 = new ArrayList<>();

        if(allProductForAdmins.getTotalElements() > 0){
            System.out.println(" co san pham trong api quan ly san pham cho admin : "+ allProductForAdmins.getTotalElements());

            allProductForAdmins.forEach(product -> {
                allProductForAdmins1.add(product);
            });

            return allProductForAdmins1;
        }else{
            System.out.println("Khoong co san pham trong api quan ly san pham cho admin");
            return null;
        }

    }
}
