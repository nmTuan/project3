package com.vuongnh.Service;

import com.vuongnh.payload.request.UpdateUserDetailsRequest;
import com.vuongnh.payload.response.UserDetails;

public interface UserService {


    UserDetails getUserInfo(Long id);

    void updateUserInfo(UpdateUserDetailsRequest updateUserDetailsRequest, Long uid);
}
