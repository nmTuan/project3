package com.vuongnh.Service.ServiceImp;

import com.vuongnh.domain.User;
import com.vuongnh.repository.RoleRepository;
import com.vuongnh.repository.UserRepository;
import com.vuongnh.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImp implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(username).
                orElseThrow(() -> new UsernameNotFoundException("Username not Found"));

        return UserPrincipal.createPrincipal(user);
    }

    // for jwt
    @Transactional
    public UserDetails loadUserById(Long id) throws UsernameNotFoundException{
        User user = userRepository.findById(id).
                orElseThrow(() -> new UsernameNotFoundException("user not founf with id : " + id));
        return UserPrincipal.createPrincipal(user);
    }
}
