package com.vuongnh.Service.ServiceImp;

import com.vuongnh.Service.CartService;
import com.vuongnh.payload.request.CartVerifyQuery;
import com.vuongnh.payload.request.CartVerifyRequest;
import com.vuongnh.payload.response.CartVerifyResponse;
import com.vuongnh.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CartServiceImp implements CartService {

    @Autowired
    ProductRepository productRepository;

    @Override
    public List<CartVerifyResponse> verifyCart(List<CartVerifyRequest> cartVerifyRequests) {


        List<CartVerifyResponse> cartVerifyResponses = new ArrayList<>();

        for (CartVerifyRequest cartVerifyRequest : cartVerifyRequests){
            CartVerifyResponse cartVerifyResponse = new CartVerifyResponse();
            CartVerifyQuery cartVerifyQuery = productRepository.getQuantityByPid(cartVerifyRequest.getPid());
            cartVerifyResponse.setPid(cartVerifyRequest.getPid());
            if (cartVerifyQuery.getQuantity() == 0){
                cartVerifyResponse.setStatus(2);
            }else if(cartVerifyQuery.getQuantity() > cartVerifyRequest.getQuantity()){
                cartVerifyResponse.setStatus(1);
            }else if(cartVerifyQuery.getQuantity() < cartVerifyRequest.getQuantity()){
                cartVerifyResponse.setStatus(3);
            }

            cartVerifyResponses.add(cartVerifyResponse);
        }

        return cartVerifyResponses;
    }
}
