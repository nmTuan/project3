package com.vuongnh.Service.ServiceImp;

import com.vuongnh.Service.SignupService;
import com.vuongnh.domain.Role;
import com.vuongnh.domain.User;
import com.vuongnh.enumClass.RoleName;
import com.vuongnh.exception.AppException;
import com.vuongnh.payload.request.SignUpRequest;
import com.vuongnh.payload.response.ApiResponse;
import com.vuongnh.repository.RoleRepository;
import com.vuongnh.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Collections;

@Service
public class SignUpServiceImp implements SignupService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public ResponseEntity<?> signUp(SignUpRequest signUpRequest) {

        if (userRepository.existsByEmail(signUpRequest.getEmail())){
            return new ResponseEntity(new ApiResponse(false,"User is already taken !"),
                    HttpStatus.OK);
        }else {

            signUpRequest.setPassword(passwordEncoder.encode(signUpRequest.getPassword()));

            User user = new User(signUpRequest.getName(),signUpRequest.getEmail(),signUpRequest.getPassword()
                                ,signUpRequest.getPhone(),signUpRequest.getAddress());

            Role role = roleRepository.findByRoleName(RoleName.ROLE_USER).
                        orElseThrow(() -> new AppException("Role is not set"));

            user.setRoles(Collections.singleton(role));

            User result = userRepository.save(user);

            URI location = ServletUriComponentsBuilder
                    .fromCurrentContextPath().path("/api/users/{email}")
                    .buildAndExpand(result.getEmail()).toUri();

            return ResponseEntity.created(location)
                    .body(new ApiResponse(true, "User registered successfully"));

//            URI location = ...;
//            HttpHeaders responseHeader = new HttpHeaders();
//            responseHeader.setLocation(location);
//            responseHeader.set("testheader","testheaderValue");
//            return new ResponseEntity<String>("This is the body",responseHeader,HttpStatus.CREATED);
        }


    }

}
