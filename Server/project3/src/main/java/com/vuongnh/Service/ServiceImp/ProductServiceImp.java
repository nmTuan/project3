package com.vuongnh.Service.ServiceImp;

import com.vuongnh.Service.ProductService;
import com.vuongnh.domain.Category;
import com.vuongnh.domain.Product;
import com.vuongnh.enumClass.CategoryName;
import com.vuongnh.payload.response.HomeProduct;
import com.vuongnh.payload.response.ProductDetails;
import com.vuongnh.repository.CategoryRepository;
import com.vuongnh.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImp implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    CategoryRepository categoryRepository;

    // get product by category each page
    @Override
    public Page<Product> findProductForHome(int page, int sizeOfPage, CategoryName categoryName) {

        System.out.println("Category name iss : " + categoryName.toString());

        Pageable pageable = PageRequest.of(page,sizeOfPage);

       return productRepository.findAllByCategory(categoryName.toString(),pageable);
    }

    @Override
    public List<HomeProduct> searchByName(String name, int page, int sizeOfPage) {

        Pageable pageable1 = PageRequest.of(page,sizeOfPage);

        Page<Product> products = productRepository.findByPnameContaining(name,pageable1);

        List<HomeProduct> homeProducts = new ArrayList<>();

        System.out.println(" name muốn tìm là : " + name);

        if (products.getTotalElements() == 0){
            System.out.println("Không tồn tại products khi SEARCH products for home ");
        }else {
            products.forEach(product -> {
                HomeProduct homeProduct = new HomeProduct();
                homeProduct.setPid(product.getPid());
                homeProduct.setPname(product.getPname());
                homeProduct.setPrice(product.getPrice());
                homeProduct.setDiscount(product.getDiscount());
                homeProduct.setImageSource(product.getImage_source());

                homeProducts.add(homeProduct);
            });
        }
        return homeProducts;
    }

    @Override
    public ProductDetails showProductDetails(Long pid) {
        return productRepository.showProductDetails(pid);
    }

    @Override
    public List<HomeProduct> getNewProduct(int page, int sizeOfPage) {

        Pageable pageable1 = PageRequest.of(page,sizeOfPage);

        Page<Product> productsForNew = productRepository.getNewProduct(pageable1);

        List<HomeProduct> homeProductsForNew = new ArrayList<>();

        if (productsForNew.getTotalElements() == 0){
            System.out.println("Không tồn tại products trong api new product ");
        }else {
            productsForNew.forEach(product -> {
                HomeProduct homeProduct1 = new HomeProduct();
                homeProduct1.setPid(product.getPid());
                homeProduct1.setPname(product.getPname());
                homeProduct1.setPrice(product.getPrice());
                homeProduct1.setDiscount(product.getDiscount());
                homeProduct1.setImageSource(product.getImage_source());

                homeProductsForNew.add(homeProduct1);
            });
        }
        return homeProductsForNew;
    }

}
