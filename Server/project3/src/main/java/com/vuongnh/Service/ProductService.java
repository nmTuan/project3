package com.vuongnh.Service;

import com.vuongnh.domain.Product;
import com.vuongnh.enumClass.CategoryName;
import com.vuongnh.payload.response.HomeProduct;
import com.vuongnh.payload.response.ProductDetails;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ProductService {

    Page<Product> findProductForHome(int page, int sizeOfPage, CategoryName categoryName);

    List<HomeProduct> searchByName(String name,int page, int sizeOfPage);

    ProductDetails showProductDetails(Long pid);

    List<HomeProduct> getNewProduct(int page, int sizeOfPage);
}
