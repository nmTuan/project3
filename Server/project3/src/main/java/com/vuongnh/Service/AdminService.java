package com.vuongnh.Service;

import com.vuongnh.payload.response.AllProductForAdmin;

import java.util.List;

public interface AdminService {

    List<AllProductForAdmin> getAllProductForAdmin(int pageIndex, int sizeOfPage);

}
