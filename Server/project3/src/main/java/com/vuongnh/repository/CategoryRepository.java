package com.vuongnh.repository;

import com.vuongnh.domain.Category;
import com.vuongnh.enumClass.CategoryName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<Category,Long> {

    Optional<Category> findByCateName(CategoryName categoryName);


    @Query(value = "SELECT ca.category_id FROM category ca WHERE ca.category_name = :categoryName", nativeQuery = true)
    int getIdByCategoryName(@Param("categoryName") CategoryName categoryName);


}
