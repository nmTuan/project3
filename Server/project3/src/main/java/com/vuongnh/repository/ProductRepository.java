package com.vuongnh.repository;

import com.vuongnh.domain.Product;
import com.vuongnh.payload.request.CartVerifyQuery;
import com.vuongnh.payload.response.AllProductForAdmin;
import com.vuongnh.payload.response.ProductDetails;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface ProductRepository extends JpaRepository<Product,Long> {

    @Query(value= "SELECT * FROM products WHERE category_id = " +
                    "(SELECT ca.category_id FROM category ca WHERE ca.category_name = ?1)",nativeQuery = true)
    Page<Product> findAllByCategory(String categoryName, Pageable pageable);

    @Query(value = "SELECT * FROM products ORDER BY updated_date DESC", nativeQuery = true)
    Page<Product> getNewProduct(Pageable pageable);


    Page<Product> findByPnameContaining(String name, Pageable pageable);


//    @Query(value = "SELECT new ProductDetails(p.pid,p.pname,p.price,p.discount,p.quantity,b.brand_name,f.info,p.description)" +
//            " FROM products p,brands b, featureproduct f WHERE p.brand_id=b.brand_id " +
//            "AND p.featured_id=f.feat_id AND p.pid =:pid")
    @Query(value="SELECT " +
            "p.pid,p.pname,p.price,p.discount,p.quantity,b.brand_name,f.info,p.description" +
            " FROM products p,brands b, featureproduct f WHERE p.brand_id=b.brand_id " +
            "AND p.featured_id=f.feat_id AND p.pid =:pid",nativeQuery = true)
    ProductDetails showProductDetails(@Param("pid") Long pid);


    @Query(value = "SELECT " +
            "p.pid,p.quantity " +
            "FROM products p " +
            "WHERE p.pid = :pid ",nativeQuery = true)
    CartVerifyQuery getQuantityByPid(@Param("pid") Long pid);

    @Query(value = "SELECT count(p.pid),p.pid,p.created_date as createdDate,p.updated_date as updatedDate,p.description,p.discount," +
            "p.image_source as imageSource,p.pname,p.price,p.quantity,b.brand_name as brand," +
            "ca.category_name as category, fea.info " +
            "FROM products p,category ca,brands b, featureproduct fea" +
            " WHERE p.brand_id = b.brand_id AND p.category_id = ca.category_id AND p.featured_id = fea.feat_id" +
            " ORDER BY p.updated_date DESC", nativeQuery = true)
    Page<AllProductForAdmin> getAllProduct(Pageable pageable);

}
