package com.vuongnh.repository;

import com.vuongnh.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {

    Optional<User> findByEmail(String email);

    Boolean existsByEmail(String email);

    Optional<User> findById(Long uid);

    @Query(value = "UPDATE user u set u.name = ?1, u.email = ?2, u.password= ?3,u.phone = ?4 , u.address= ?5 WHERE u.uid = ?6",nativeQuery = true)
    void updateUserInfo(String name,String email, String password, String phone, String address, Long uid);

}
