package com.vuongnh.controller;

import com.vuongnh.Service.UserService;
import com.vuongnh.payload.request.UpdateUserDetailsRequest;
import com.vuongnh.payload.response.UserDetails;
import com.vuongnh.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/api")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping(value = "/userInfo")
    public ResponseEntity<?> getUserInfo(){

        UserPrincipal userPrincipal = getUserPrincipal();

        return ResponseEntity.ok(userService.getUserInfo(userPrincipal.getId()));

    }

    @PutMapping(value = "/updateUserInfo")
    public ResponseEntity<?> updateUserInfo(@RequestBody UpdateUserDetailsRequest updateUserDetailsRequest){

        try{
            userService.updateUserInfo(updateUserDetailsRequest,getUserPrincipal().getId());
            return ResponseEntity.ok("cập nhập thông tin user Thành Công !");
        }catch(Exception e ){
            return ResponseEntity.ok("cập nhập thông tin user Thất Bại !");
        }

    }

    private UserPrincipal getUserPrincipal(){

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null
                || !authentication.isAuthenticated()
                || authentication instanceof AnonymousAuthenticationToken){
            ResponseEntity.ok("Cần đăng nhập mới có thể lấy thông tin user");
        }

        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();

        return userPrincipal;
    }
}
