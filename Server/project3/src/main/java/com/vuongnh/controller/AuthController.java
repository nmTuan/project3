package com.vuongnh.controller;

import com.vuongnh.payload.request.LoginRequest;
import com.vuongnh.payload.response.ApiResponse;
import com.vuongnh.payload.response.JwtAuthenticationResponse;
import com.vuongnh.security.JwtProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtProvider jwtProvider;

    @PostMapping("/login")
    public ResponseEntity<?> login(@Valid @RequestBody LoginRequest loginRequest){

        Authentication authentication  = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getEmail(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtProvider.generateJwt(authentication);

        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt,true));

    }

    @GetMapping("/checkLogin")
    public ResponseEntity<?> checkLogin(){
        return ResponseEntity.ok(new ApiResponse(true,"Người đã đăng nhập mới thấy được !! "));
    }
}
