package com.vuongnh.controller;

import com.vuongnh.Service.CartService;
import com.vuongnh.payload.request.CartVerifyRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class CartController {

    @Autowired
    CartService cartService;

        @PostMapping("/cart/checkCart")
        public ResponseEntity<?> checkCart(@RequestBody List<CartVerifyRequest> cartVerifyRequests){

            if (cartVerifyRequests.size() != 0){
                return ResponseEntity.ok(cartService.verifyCart(cartVerifyRequests));
            }else {
                return ResponseEntity.ok("Không có sản phẩm nào trong giỏ hàng");
            }
        }

}
