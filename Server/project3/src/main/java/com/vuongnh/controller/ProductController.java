package com.vuongnh.controller;

import com.vuongnh.Service.ProductService;
import com.vuongnh.domain.Product;
import com.vuongnh.enumClass.CategoryName;
import com.vuongnh.payload.request.SearchProduct;
import com.vuongnh.payload.response.HomeProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping(value = "/productForHome")
    public ResponseEntity<?> getLap(@RequestParam("CategoryName") String categoryName,
                                    @RequestParam("pageIndex") Integer pageIndex,
                                    @RequestParam("sizeOfPage")  Integer sizeOfPage){

        try {
            System.out.println(CategoryName.valueOf(categoryName.trim().toUpperCase()));
            Page<Product> products = productService.findProductForHome( pageIndex, sizeOfPage ,
                    CategoryName.valueOf(categoryName.trim().toUpperCase()));

            List<HomeProduct> productForHome = new ArrayList<>();

            if (products.getTotalElements() == 0){
                    System.out.println("Không tồn tại products khi tìm products for home ");
            }else {
                System.out.println("có products !");
                long sumProduct = products.getTotalElements();
                products.forEach(product -> {
                    HomeProduct homeProduct = new HomeProduct();

                    homeProduct.setPid(product.getPid());
                    homeProduct.setPname(product.getPname());
                    homeProduct.setDiscount(product.getDiscount());
                    homeProduct.setPrice(product.getPrice());
                    homeProduct.setImageSource(product.getImage_source());
                    homeProduct.setSumProduct(sumProduct);

                    productForHome.add(homeProduct);

                });
            }

            return ResponseEntity.ok(productForHome);
        }catch (IllegalArgumentException ex){
            return ResponseEntity.ok("Loại product từ client react gửi lên không có trong database !" );
        }
    }

    @GetMapping(value = "/newProduct")
    public ResponseEntity<?> getNewProduct(){

      return ResponseEntity.ok(productService.getNewProduct(0,5));

    }



//    @PostMapping(value = "/searchByName",consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
//    public ResponseEntity<?> searchByName(@RequestParam(required = false,value = "name") String name,
//                                          @RequestParam(required = false,value = "page") Integer page,
//                                          @RequestParam(required = false,value = "sizeOfPage") Integer sizeOfPage ){
//
////        List<HomeProduct> products = productService.searchByName(searchProduct.getName(),
////                searchProduct.getPage(), searchProduct.getSizeOfPage());
////        List<HomeProduct> products = productService.searchByName(paramMap.get("name").toString(),
////                (int)paramMap.get("page"),(int)paramMap.get("sizeOfPage"));
//        List<HomeProduct> products = productService.searchByName(name,page,sizeOfPage);
//
//        return ResponseEntity.ok(products);
//
//    }
    @PostMapping(value = "/searchByName")
    public ResponseEntity<?> searchByName(@Valid @RequestBody SearchProduct searchProduct){

        List<HomeProduct> products = productService.searchByName(searchProduct.getName(),
                searchProduct.getPage(), searchProduct.getSizeOfPage());

        return ResponseEntity.ok(products);

    }

    @GetMapping(value = "/detailProduct/{pid}")
    public ResponseEntity<?> showProductDetails(@PathVariable(name = "pid") Long pid){
        try{
            if(productService.showProductDetails(pid) == null){
                ResponseEntity.ok("Không có detail product ứng với pid được truyền vào .");
            }
            return ResponseEntity.ok(productService.showProductDetails(pid));
        }catch (Exception e){
            return ResponseEntity.ok("Xem detail product lỗi !");
        }
    }

}
