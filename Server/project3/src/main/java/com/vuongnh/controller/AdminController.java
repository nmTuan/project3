package com.vuongnh.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vuongnh.Service.AdminService;
import com.vuongnh.common.Const;
import com.vuongnh.payload.request.FormAddProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

@RestController
@RequestMapping(value = "/api")
public class AdminController {

    @Autowired
    AdminService adminService;

    @GetMapping(value = "/allProductForAdmin")
    public ResponseEntity<?> getAllProductForAdmin(@RequestParam(name = "pageIndex") Integer pageIndex,
                                                    @RequestParam(name="sizeOfPage") Integer sizeOfPage){

        return ResponseEntity.ok(adminService.getAllProductForAdmin(pageIndex,sizeOfPage));
    }

    @RequestMapping(value = "/addproduct", method = RequestMethod.POST
            ,consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public void addProduct(@RequestParam("file") MultipartFile file,
                           @RequestParam String formAddProduct) throws IOException {

        File convertFile = new File(Const.UPLOAD_IMAGE + file.getOriginalFilename());
        System.out.println(file.getOriginalFilename());
        convertFile.createNewFile();
        FileOutputStream fout = new FileOutputStream(convertFile);
        fout.write(file.getBytes());
        fout.close();
        System.out.println("upload thanh cong" + formAddProduct);
        ObjectMapper mapper = new ObjectMapper();
        FormAddProduct readValue = mapper.readValue(formAddProduct, FormAddProduct.class);
        System.out.println(readValue.getId() + readValue.getName());

    }

}
