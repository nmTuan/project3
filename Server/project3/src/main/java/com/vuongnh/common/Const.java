package com.vuongnh.common;

public class Const {
    public static final String UPLOAD_IMAGE = "src/main/resources/static/images/";
    public static final String UPLOAD_PDF = "src/main/resources/static/upload/content";
    public static final String UPLOAD_AVATAR = "src/main/resources/static/upload/avatar";
}
