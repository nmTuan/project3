package com.vuongnh.dataInit;

import com.vuongnh.domain.Category;
import com.vuongnh.domain.Role;
import com.vuongnh.domain.User;
import com.vuongnh.enumClass.CategoryName;
import com.vuongnh.enumClass.RoleName;
import com.vuongnh.repository.CategoryRepository;
import com.vuongnh.repository.RoleRepository;
import com.vuongnh.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class DataSeedingListener implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        if (!roleRepository.findByRoleName(RoleName.ROLE_ADMIN).isPresent()){
            roleRepository.save(new Role(RoleName.ROLE_ADMIN));
        }
        if (!roleRepository.findByRoleName(RoleName.ROLE_USER).isPresent()){
            roleRepository.save(new Role(RoleName.ROLE_USER));
        }

        if(!userRepository.findByEmail("admin@gmail.com").isPresent()){
            User admin = new User("Admin Nguyễn", "admin@gmail.com", null,
                    "0976288654","Lương Tài, Bắc Ninh");
            admin.setPassword(passwordEncoder.encode("admin123456"));
            Set<Role> roles = new HashSet<>();
            roles.add(roleRepository.findByRoleName(RoleName.ROLE_ADMIN).get());
            roles.add(roleRepository.findByRoleName(RoleName.ROLE_USER).get());
            admin.setRoles(roles);

            userRepository.save(admin);
        }
        if(!userRepository.findByEmail("firstmember@gmail.com").isPresent()){
            User user = new User("Member Nguyễn", "firstmember@gmail.com",null,
                    "0998627188","Quảng Bố,Lương Tài,Bắc Ninh");
            user.setPassword(passwordEncoder.encode("member123456"));
            Set<Role> roles = new HashSet<>();
            roles.add(roleRepository.findByRoleName(RoleName.ROLE_USER).get());
            user.setRoles(roles);

            userRepository.save(user);
        }

        if(!categoryRepository.findByCateName(CategoryName.LAPTOP).isPresent()){
            categoryRepository.save(new Category(CategoryName.LAPTOP));
        }
        if(!categoryRepository.findByCateName(CategoryName.PHONE).isPresent()){
            categoryRepository.save(new Category(CategoryName.PHONE));
        }
        if(!categoryRepository.findByCateName(CategoryName.TABLET).isPresent()){
            categoryRepository.save(new Category(CategoryName.TABLET));
        }
        if(!categoryRepository.findByCateName(CategoryName.DEVICE).isPresent()){
            categoryRepository.save(new Category(CategoryName.DEVICE));
        }
    }
}
