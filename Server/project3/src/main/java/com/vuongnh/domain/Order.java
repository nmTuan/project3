package com.vuongnh.domain;

import com.vuongnh.domain.Audit.UserDateAudit;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "orders")
public class Order extends UserDateAudit {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "oid", nullable = false)
    private Long oid;


    @Column(name = "shipping_address", nullable = false)
    private String shipAddress;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    private Set<ProductOrder>  productOrders;

//    @ManyToMany
//    @JoinTable(
//            name = "order_product",
//            joinColumns = @JoinColumn(name = "product_id"),
//            inverseJoinColumns = @JoinColumn(name = "order_id"),
//
//    )
//    private Set<Product> products;


    public Long getOid() {
        return oid;
    }

    public void setOid(Long oid) {
        this.oid = oid;
    }

    public String getShipAddress() {
        return shipAddress;
    }

    public void setShipAddress(String shipAddress) {
        this.shipAddress = shipAddress;
    }

    public Set<ProductOrder> getProductOrders() {
        return productOrders;
    }

    public void setProductOrders(Set<ProductOrder> productOrders) {
        this.productOrders = productOrders;
    }
}
