package com.vuongnh.domain;

import com.vuongnh.domain.Audit.UserDateAudit;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "comments")
public class Comment extends UserDateAudit {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long cmt_id;

    @NotEmpty
    @Column(name = "contentcmt")
    private String contentCmt;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    public Long getCmt_id() {
        return cmt_id;
    }

    public void setCmt_id(Long cmt_id) {
        this.cmt_id = cmt_id;
    }

    public String getContentCmt() {
        return contentCmt;
    }

    public void setContentCmt(String contentCmt) {
        this.contentCmt = contentCmt;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
