package com.vuongnh.domain;

import com.vuongnh.domain.Audit.UserDateAudit;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name = "products")
public class Product extends UserDateAudit {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pid", nullable = false)
    private Long pid;

    @NotNull
    @Column(name = "pname", nullable = false)
    private String pname;

    @NotNull
    @Column(name = "image_source", nullable = false)
    private String imageSource;

    @NotNull
    @Min(value = 1)
    @Column(name = "price")
    private Double price;

    @NotNull
    @Min(value = 1)
    @Column(name = "quantity")
    private Long quantity;

    @Column(name = "discount")
    private Double discount;

    @Column(name = "description")
    private String description;

    @OneToOne
    @JoinColumn(name = "featured_id")
    private ProductFeature featured; // quan he one to one voi product feature

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "category_id")
    private Category category; // quan he many to one voi bang category

    @ManyToOne
    @JoinColumn(name = "brand_id")
    private Brand brand; // quan he many to one voi bang brand

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private Set<Comment> comments; // quan he one to many voi bang comment

//    @ManyToMany(mappedBy = "products", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    private Set<Order> orders;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private Set<ProductOrder>  productOrders;

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getImage_source() {
        return imageSource;
    }

    public void setImage_source(String image_source) {
        this.imageSource = image_source;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ProductFeature getFeatured() {
        return featured;
    }

    public void setFeatured(ProductFeature featured) {
        this.featured = featured;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Set<Comment> getComment() {
        return comments;
    }

    public void setComment(Set<Comment> comments) {
        this.comments = comments;
    }

    public String getImageSource() {
        return imageSource;
    }

    public void setImageSource(String imageSource) {
        this.imageSource = imageSource;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public Set<ProductOrder> getProductOrders() {
        return productOrders;
    }

    public void setProductOrders(Set<ProductOrder> productOrders) {
        this.productOrders = productOrders;
    }
}