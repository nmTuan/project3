package com.vuongnh.domain;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "product_order")
public class ProductOrder {


    @EmbeddedId
    private ProductOrderId productOrderId;

    @ManyToOne
    @MapsId("productId")
    private Product product;

    @ManyToOne
    @MapsId("orderId")
    private Order order;

    @NotNull
    @Min(value = 1)
    @Column(name = "quantity")
    private Long quantity;

    @Column(name = "discount")
    private Double discount;

    @NotNull
    @Min(value = 1)
    @Column(name = "price")
    private Double price;

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
