package com.vuongnh.domain;

import com.vuongnh.enumClass.CategoryName;
import com.vuongnh.repository.CategoryRepository;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name = "category")
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "category_id", nullable = false)
    private Long cate_id;

    @Enumerated(EnumType.STRING)
    @NaturalId
    @Column(name = "category_name")
    private CategoryName cateName;

    @OneToMany(mappedBy = "category",cascade = CascadeType.ALL)
    private Set<Product> products;

    public Category(CategoryName categoryName){
        this.cateName = categoryName;
    }
    public Category( Long cate_id){
        this.cate_id = cate_id;
    }

    public Category(){

    }

    public Long getCate_id() {
        return cate_id;
    }

    public void setCate_id(Long cate_id) {
        this.cate_id = cate_id;
    }

    public CategoryName getCate_name() {
        return cateName;
    }

    public void setCate_name(CategoryName cate_name) {
        this.cateName = cate_name;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }
}
