package com.vuongnh.domain;

import javax.persistence.*;

@Entity
@Table(name = "featureproduct")
public class ProductFeature {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "feat_id")
    private Long featproduct_id;

    @Column(name = "info")
    private String info;

    public Long getFeatproduct_id() {
        return featproduct_id;
    }

    public void setFeatproduct_id(Long featproduct_id) {
        this.featproduct_id = featproduct_id;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
