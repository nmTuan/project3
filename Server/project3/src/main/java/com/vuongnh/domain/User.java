package com.vuongnh.domain;

import com.vuongnh.domain.Audit.DateAudit;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "user", uniqueConstraints = {
        @UniqueConstraint(columnNames = "email"),
        @UniqueConstraint(columnNames = "phone")
})
public class User extends DateAudit {
    private static final long serialVersionUID = 1L;


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "uid",nullable = false)
    private Long uid;

    @Column(name = "name", nullable = false)
    @NotBlank
    @Size(max = 40)
    private String name;

    @NaturalId
    @Column(name = "email",nullable = false)
    @NotBlank
    @Size(max = 25)
    @Email
    private String email;

    @NotBlank
    @Column(name = "password", nullable = false)
    @Size(min = 8,max = 255)
    private String password;

    @NotBlank
    @Column(name = "phone", nullable = false)
    @Pattern(regexp = "(\\+84|0)[0-9]{9}")
    private String phone;

    @Column(name = "address", nullable = false)
    @Size(max = 40)
    private String address;

    @ManyToMany
    @JoinTable(
            name = "user_role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private Set<Role> roles;

    public User() {

    }

    public User(@NotBlank @Size(max = 40) String name, @NotBlank @Size(max = 25) @Email String email,
                @NotBlank @Size(min = 8, max = 40) String password, @NotBlank @Pattern(regexp = "(\\+84|0)[1-9]{9}")
                        String phone, @Size(max = 200) String address) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.address = address;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}
