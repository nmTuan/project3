package com.vuongnh.payload.request;

import org.hibernate.annotations.NaturalId;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class SignUpRequest {

    @NotBlank
    @Size(max = 40)
    private String name;

    @NaturalId
    @NotBlank
    @Size(max = 25)
    @Email
    private String email;

    @NotBlank
    @Size(min = 8,max = 255)
    private String password;

    @NotBlank
    @Pattern(regexp = "(\\+84|0)[1-9]{9}")
    private String phone;

    @Size(max = 40)
    private String address;

    public SignUpRequest(@NotBlank @Size(max = 40) String name, @NotBlank @Size(max = 25) @Email String email,
                         @NotBlank @Size(min = 8, max = 255) String password, @NotBlank @Pattern(regexp = "(\\+84|0)[1-9]{9}") String phone, @Size(max = 40) String address) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.address = address;
    }

    public SignUpRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
