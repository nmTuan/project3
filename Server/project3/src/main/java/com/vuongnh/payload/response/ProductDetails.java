package com.vuongnh.payload.response;

public interface ProductDetails {


    // Spring Projection

    // using Spring Projection to custom object returned in jparepository

    Long getPid();

    String getPname();

    Double getPrice();

    Double getDiscount();

    Long getQuantity();

    String getBrand_name();

    String getInfo();

    String getDescription();

//    private Long pid;
//
//    private String pname;
//
//    private Double price;
//
//    private Double discount;
//
//    private Long quantity;
//
//    private String brand_name;
//
//    private String info;
//
//    private String description;
//
//    public ProductDetails(Long pid, String pname, Double price, Double discount,
//                          Long quantity, String brand_name, String info, String description) {
//        this.pid = pid;
//        this.pname = pname;
//        this.price = price;
//        this.discount = discount;
//        this.quantity = quantity;
//        this.brand_name = brand_name;
//        this.info = info;
//        this.description = description;
//    }
//
//    public Long getPid() {
//        return pid;
//    }
//
//    public void setPid(Long pid) {
//        this.pid = pid;
//    }
//
//    public String getPname() {
//        return pname;
//    }
//
//    public void setPname(String pname) {
//        this.pname = pname;
//    }
//
//    public Double getPrice() {
//        return price;
//    }
//
//    public void setPrice(Double price) {
//        this.price = price;
//    }
//
//    public Double getDiscount() {
//        return discount;
//    }
//
//    public void setDiscount(Double discount) {
//        this.discount = discount;
//    }
//
//    public Long getQuantity() {
//        return quantity;
//    }
//
//    public void setQuantity(Long quantity) {
//        this.quantity = quantity;
//    }
//
//    public String getBrand_name() {
//        return brand_name;
//    }
//
//    public void setBrand_name(String brand_name) {
//        this.brand_name = brand_name;
//    }
//
//    public String getInfo() {
//        return info;
//    }
//
//    public void setInfo(String info) {
//        this.info = info;
//    }
//
//    public String getDescription() {
//        return description;
//    }
//
//    public void setDescription(String description) {
//        this.description = description;
//    }



}
