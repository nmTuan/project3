package com.vuongnh.payload.response;

import java.time.Instant;

public interface AllProductForAdmin {

    Long getPid();

    Instant getCreatedDate();

    Instant getUpdatedDate();

    String getDescription();

    Integer getDiscount();

    String getImageSource();

    String getPname();

    Double getPrice();

    Double getQuantity();

    String getBrand();

    String getCategory();

    String getInfo();
}
