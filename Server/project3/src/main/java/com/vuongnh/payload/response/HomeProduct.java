package com.vuongnh.payload.response;

public class HomeProduct {

    private Long pid;

    private String pname;

    private Double price;

    private Double discount;

    private String imageSource;

    private long sumProduct;

    public long getSumProduct() {
        return sumProduct;
    }

    public void setSumProduct(long sumProduct) {
        this.sumProduct = sumProduct;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public String getImageSource() {
        return imageSource;
    }

    public void setImageSource(String imageSource) {
        this.imageSource = imageSource;
    }
}
