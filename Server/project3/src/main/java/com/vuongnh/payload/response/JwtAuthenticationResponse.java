package com.vuongnh.payload.response;

public class JwtAuthenticationResponse {

    private String jwt;

    private String token = "Bearer ";

    private Boolean status;

    public JwtAuthenticationResponse(String jwt, Boolean status) {
        this.jwt = jwt;
        this.status = status;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
