package com.vuongnh.payload.response;

public class CartVerifyResponse {


    private Long pid;

    private int status;

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
