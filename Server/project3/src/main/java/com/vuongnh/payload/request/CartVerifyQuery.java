package com.vuongnh.payload.request;

public interface CartVerifyQuery {

    Long getPid();

    Long getQuantity();

}
