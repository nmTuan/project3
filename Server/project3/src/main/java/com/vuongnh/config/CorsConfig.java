package com.vuongnh.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebSecurity
public class CorsConfig implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry corsRegistry){

//        corsRegistry.addMapping("/**")
//                .allowedMethods("HEAD", "GET", "PUT", "POST", "DELETE", "PATCH");
        corsRegistry.addMapping("/api/auth/login");
        corsRegistry.addMapping("api/signUp");
        corsRegistry.addMapping("/api/auth/checkLogin");
        corsRegistry.addMapping("/api/**");

//        corsRegistry.addMapping("/**").allowedOrigins("*").allowedMethods("GET", "POST", "OPTIONS", "PUT")
//                .allowedHeaders("Content-Type", "X-Requested-With", "accept", "Origin", "Access-Control-Request-Method",
//                        "Access-Control-Request-Headers","Access-Control-Allow-Origin")
//                .exposedHeaders("Access-Control-Allow-Credentials")
//                .allowCredentials(true).maxAge(3600);
    }

}
