//package com.vuongnh.config;
//
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.servlet.config.annotation.*;
//
////@Configuration
////public class StaticResourceConfiguration extends WebMvcConfigurerAdapter {
////
////    private static final String[] CLASSPATH_RESOURCE_LOCATIONS = {
////            "classpath:/META-INF/resources/", "classpath:/resources/",
////            "classpath:/static/", "classpath:/public/" };
////
////    @Override
////    public void addResourceHandlers(ResourceHandlerRegistry registry) {
////        registry.addResourceHandler("/**")
////                .addResourceLocations(CLASSPATH_RESOURCE_LOCATIONS);
////    }
////}
//
//// Link to read config static resource cause using @EnableMvcWeb disable default config of Spring boot application
//// LINK: https://stackoverflow.com/questions/24661289/spring-boot-not-serving-static-content
//
////@Configuration
////public class StaticResourceConfiguration extends WebMvcConfigurerAdapter  {
////    @Override
////    public void addResourceHandlers(ResourceHandlerRegistry registry) {
////        registry
////                .addResourceHandler("/resources/**")
////                .addResourceLocations("/resources/");
////    }
////}
//
//// Link : https://stackoverflow.com/questions/41691770/spring-boot-unabe-to-serve-static-image-from-resource-folder
//@Configuration
//public class StaticResourceConfiguration extends WebMvcConfigurationSupport {
//
//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry){
//        registry.addResourceHandler("/**")
//                .addResourceLocations("classpath:/static/");
//    }
//}