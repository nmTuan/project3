package firstcodemongodb.eshop.service;

import firstcodemongodb.eshop.domain.User;
import firstcodemongodb.eshop.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public void saveAll(Iterable<User> iterable){
        userRepository.saveAll(iterable);
    }

    public void save(User user){
        userRepository.save(user);
    }

    public void deleteById(String id){
        userRepository.deleteById(id);
    }

    public void deleteByName(String name){
        userRepository.deleteByName(name);
    }
}
