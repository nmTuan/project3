package firstcodemongodb.eshop.repository;

import firstcodemongodb.eshop.domain.User;
import org.springframework.data.domain.Example;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends MongoRepository<User,String> {

    @Override
    <S extends User> List<S> saveAll(Iterable<S> iterable);

    @Override
    <S extends User> S save(S s);

    void deleteByName(String name);

    @Override
    void deleteById(String s);

    @Override
    <S extends User> S insert(S s);

}
