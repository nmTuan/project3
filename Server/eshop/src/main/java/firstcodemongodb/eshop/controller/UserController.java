package firstcodemongodb.eshop.controller;

import firstcodemongodb.eshop.domain.User;
import firstcodemongodb.eshop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(
        value = "/api/user/"
)
@RestController
public class UserController {

    @Autowired
    UserService userService;

//    @PostMapping(value = "/saveUsers")
//    public ResponseEntity<?> saveUsers(@RequestBody User user){
////        Iterable<User> iterable = users;
//        try{
//            userService.save(user);
//            return ResponseEntity.ok("save user sucessful !");
//        }catch (Exception ex){
//            ex.printStackTrace();
//            return ResponseEntity.ok("save user failed ! !");
//        }
//
//    }

    @PostMapping(value = "/saveUsers")
    public ResponseEntity<?> saveUsers(@RequestBody List<User> users){
       Iterable<User> iterable = users;
        try{
            userService.saveAll(users);
            return ResponseEntity.ok("save user sucessful !");
        }catch (Exception ex){
            ex.printStackTrace();
            return ResponseEntity.ok("save user failed ! !");
        }

    }
    @DeleteMapping(value = "deleteUser")
    public ResponseEntity<?> deleteUserById(@RequestParam(required = false) String id,
                                            @RequestParam(required = false) String name) {

        try {
            if (id != null && name == null) {
                userService.deleteById(id);
                return ResponseEntity.ok("delete user  have id : " + id + " sucessful !");
            } else if (id == null && name != null) {
                userService.deleteByName(name);
                return ResponseEntity.ok("delete user  have name : " + name + " sucessful !");
            }
            return ResponseEntity.ok("chua xu ly");
        } catch (Exception ex) {
            ex.printStackTrace();
            return ResponseEntity.ok("delete user  failed !");
        }

    }

}
