package firstcodemongodb.eshop.config;

import com.mongodb.client.MongoClient;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;

public class ConnectMongoDB extends AbstractMongoClientConfiguration {

    @Override
    public MongoClient mongoClient() {
        return null;
    }

    @Override
    protected String getDatabaseName() {
        return "eTechShop";
    }

}
